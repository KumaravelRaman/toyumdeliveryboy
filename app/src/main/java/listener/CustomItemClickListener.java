package listener;

import android.view.View;

/**
 * Created by SYSTEM02 on 12/8/2017.
 */

public interface CustomItemClickListener {
    public void onItemClick();
}
