package fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;

import com.brainmagic.toyumwater.R;

import java.util.ArrayList;
import java.util.List;

import apiservice.APIService;
import adapter.FollowListadapter;


import model.Telecaller.FollowCustomerList.Datum;
import model.Telecaller.FollowCustomerList.FollowList;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SYSTEM02 on 11/20/2017.
 */

public class TelecallerTwo extends Fragment {

  //  Alert alert;
    NetworkConnection network;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private RecyclerView recyclerView;
    private HorizontalScrollView Horizontalscrollview;
    private List<Datum> GeneralList;

    private Toasts toster;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.telecaller2, container, false);

        //alert = new Alert(getActivity());
        network = new NetworkConnection(getActivity());
        toster  = new Toasts(getActivity());

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view2);
        Horizontalscrollview = (HorizontalScrollView) rootView.findViewById(R.id.HorizontalScrollView);
        GeneralList = new ArrayList<>();
        myshare = getActivity().getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        if(network.CheckInternet())
        {
            Get_GeneralList();
        }else{
            Horizontalscrollview.setVisibility(View.INVISIBLE);
            //alert.showAlertbox(getString(R.string.connection_slow));
            toster.ShowErrorToast(getString(R.string.no_network));
        }




        return rootView;
    }
    MyReceiver r;
    public void refresh() {
        //yout code in refresh.
        Log.i("Refresh", "YES");
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(r);
    }

    public void onResume() {
        super.onResume();
        r = new MyReceiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(r,
                new IntentFilter("TAG_REFRESH2"));
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(network.CheckInternet())
            {
                Get_GeneralList();
                TelecallerTwo.this.refresh();
            }else{
                //alert.showAlertbox(getString(R.string.connection_slow));
                //TelecallerTwo.this.refresh();
                Horizontalscrollview.setVisibility(View.INVISIBLE);
                toster.ShowErrorToast(getString(R.string.no_network));
            }


        }
    }

    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Get_GeneralList();// Refresh your fragment here
        }
    }*/
  /*  @Override
    public void onResume()
    {
        super.onResume();
        Get_GeneralList();// Load data and do stuff
    }*/

    /*@Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Get_GeneralList();
    }*/


/*    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);


        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Get_GeneralList();
                _hasLoadedOnce = true;
            }
        }
    }*/

    public void Get_GeneralList(){

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.v("Follow List","loading");
        APIService service = RetroClient.getApiService();

        Call<FollowList> call = service.FOLLOW_LIST_CALL(String.format("%s",myshare.getLong(Shared.K_Id,0))
                /*"name",
                "All",
                Integer.parseInt("0"),
                Integer.parseInt("0")
*/
        );
        call.enqueue(new Callback<FollowList>() {
            @Override
            public void onResponse(Call<FollowList> call, Response<FollowList> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                    {
                        GeneralList = response.body().getData();
                        FollowListadapter mAdapter = new FollowListadapter(getActivity(), GeneralList, TelecallerTwo.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        Horizontalscrollview.setVisibility(View.VISIBLE);
                        break;
                    }
                    case "No data":
                    {
                        //recyclerView.setVisibility(View.INVISIBLE);//alert.showAlertbox("No Installations found");
                        Horizontalscrollview.setVisibility(View.INVISIBLE);//alert.showAlertbox("No Installations found")
                        toster.ShowErrorToast("Sorry! No FollowUps found");
                        /*AlertDialogue alertnew = new AlertDialogue(getActivity());
                        Horizontalscrollview.setVisibility(View.GONE);
                        alertnew.showAlertbox("Dear Executive, your Customer list is empty");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                //finish();
                            }
                        });
                        *///alert.
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                    default: {
                        //alert.showAlertbox(getString(R.string.connection_slow));
                        //Horizontalscrollview.setVisibility(View.GONE);
                        //Body_layout.setVisibility(View.GONE);
                        Horizontalscrollview.setVisibility(View.INVISIBLE);
                        toster.ShowErrorToast(getString(R.string.connection_slow));
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<FollowList> call, Throwable t) {
                progressDialog.dismiss();
                toster.ShowErrorToast(getString(R.string.connection_slow));
                //alert.showAlertbox(getString(R.string.connection_slow));
                // Horizontalscrollview.setVisibility(View.GONE);

            }
        });

    }

}




 /*
    private Context context;
    private RadioGroup radioGroup;

    private RadioButton radioButton1, radioButton2;
    private Button Sumbit;
    private String Alphabet = "All";
    private EditText From,To;
    Toasts Toaster;
    private LinearLayout NumberLayout,NameLayout;



 radioGroup = (RadioGroup) rootView.findViewById(R.id.radiogroup);
        radioButton1 = (RadioButton) rootView.findViewById(R.id.radioButton1);
        radioButton2 = (RadioButton) rootView.findViewById(R.id.radioButton2);
        Sumbit = (Button) rootView.findViewById(R.id.submit);
        From = (EditText) rootView.findViewById(R.id.from);
        To = (EditText) rootView.findViewById(R.id.to);
         Toaster = new Toasts(getActivity());
        NumberLayout = (LinearLayout) rootView.findViewById(R.id.numberlayout);
        NameLayout = (LinearLayout) rootView.findViewById(R.id.namelayout);

        final CarouselPicker carouselPicker = (CarouselPicker) rootView.findViewById(R.id.carousel);
        final List<CarouselPicker.PickerItem> textItems = new ArrayList<>();
//20 here represents the textSize in dp, change it to the value you want.
        textItems.add(new CarouselPicker.TextItem("All", 20));
        textItems.add(new CarouselPicker.TextItem("A", 20));
        textItems.add(new CarouselPicker.TextItem("B", 20));
        textItems.add(new CarouselPicker.TextItem("C", 20));
        textItems.add(new CarouselPicker.TextItem("D", 20));
        textItems.add(new CarouselPicker.TextItem("E", 20));
        textItems.add(new CarouselPicker.TextItem("F", 20));
        textItems.add(new CarouselPicker.TextItem("G", 20));
        textItems.add(new CarouselPicker.TextItem("H", 20));
        textItems.add(new CarouselPicker.TextItem("I", 20));
        textItems.add(new CarouselPicker.TextItem("J", 20));
        textItems.add(new CarouselPicker.TextItem("K", 20));
        textItems.add(new CarouselPicker.TextItem("L", 20));
        textItems.add(new CarouselPicker.TextItem("M", 20));
        textItems.add(new CarouselPicker.TextItem("N", 20));
        textItems.add(new CarouselPicker.TextItem("O", 20));
        textItems.add(new CarouselPicker.TextItem("P", 20));
        textItems.add(new CarouselPicker.TextItem("Q", 20));
        textItems.add(new CarouselPicker.TextItem("R", 20));
        textItems.add(new CarouselPicker.TextItem("S", 20));
        textItems.add(new CarouselPicker.TextItem("T", 20));
        textItems.add(new CarouselPicker.TextItem("U", 20));
        textItems.add(new CarouselPicker.TextItem("V", 20));
        textItems.add(new CarouselPicker.TextItem("W", 20));
        textItems.add(new CarouselPicker.TextItem("X", 20));
        textItems.add(new CarouselPicker.TextItem("Y", 20));
        textItems.add(new CarouselPicker.TextItem("Z", 20));

        CarouselPicker.CarouselViewAdapter textAdapter = new CarouselPicker.CarouselViewAdapter(getActivity(), textItems, 0);
        carouselPicker.setAdapter(textAdapter);

        carouselPicker.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Alphabet = textItems.get(position).getText();
                Toast.makeText(getActivity(), Alphabet, Toast.LENGTH_SHORT).show();
                //position of the selected item
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButton1) {
                    NameLayout.setVisibility(View.VISIBLE);
                    NumberLayout.setVisibility(View.GONE);
                } else if(checkedId == R.id.radioButton2) {
                    NumberLayout.setVisibility(View.VISIBLE);
                    NameLayout.setVisibility(View.GONE);
                } else {


                }

            }
        });
        Sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();

                if(selectedId == radioButton1.getId()) {
                    //Toast.makeText(getActivity(), "Alphabet", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), Telecaller_View_Customers_Activity.class)
                            .putExtra("filtertype","name")
                            .putExtra("start","")
                            .putExtra("end","")
                            .putExtra("alpha",Alphabet)
                            .putExtra("result","Alphabet "+Alphabet)
                    );

                } else if(selectedId == radioButton2.getId()) {
                    //Toast.makeText(getActivity(), "Numbers", Toast.LENGTH_SHORT).show();
                    if(From.getText().toString().equals(""))
                        Toaster.ShowErrorToast("Please enter From value");
                    else if(To.getText().toString().equals(""))
                        Toaster.ShowErrorToast("Please enter To value");
                    else if(From.getText().toString().equals("0"))
                        Toaster.ShowErrorToast("Enter valid From value");
                    else if(To.getText().toString().equals("0"))
                        Toaster.ShowErrorToast("Enter valid To value");
                    else if(Integer.parseInt(To.getText().toString())  < Integer.parseInt(From.getText().toString()))
                    {
                        Toaster.ShowErrorToast("To value should be greater than From");
                    }
                    else {


                        startActivity(new Intent(getActivity(), Telecaller_View_Customers_Activity.class)
                                .putExtra("filtertype","number")
                                .putExtra("start",From.getText().toString())
                                .putExtra("end",To.getText().toString())
                                .putExtra("alpha","")
                                .putExtra("result","Number "+From.getText().toString()+" to "+To.getText().toString())
                        );

                    }

                } else {

                    Toaster.ShowErrorToast("Select Filter option");

                }


            }
        });*/
