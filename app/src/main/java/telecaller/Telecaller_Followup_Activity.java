package telecaller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;
import fragments.TelecallerOne;
import fragments.TelecallerTwo;
import logout.logout;
import sharedpreference.Shared;
import viewpager.CustomViewPager;

public class Telecaller_Followup_Activity extends AppCompatActivity {

    CustomViewPager mViewPager;
    TabLayout tabLayout;
    CustomPagerAdapter mCustomPagerAdapter;
    TextView Name;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    //private static final int NUM_PAGES = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telecaller_followup);

        tabLayout = (TabLayout)findViewById(R.id.table_layout);
        Name = (TextView) findViewById(R.id.name);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();
        Name.setText(myshare.getString(Shared.K_Name,""));


        tabLayout.addTab(tabLayout.newTab().setText("Follow Up"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mViewPager = (CustomViewPager) findViewById(R.id.pager);

        mCustomPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        //mViewPager.setOffscreenPageLimit(0);
        mViewPager.setAdapter(mCustomPagerAdapter);

      /*  ViewPager.OnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(Telecaller_Followup_Activity.this,
                        "Selected page position: " + position, Toast.LENGTH_SHORT).show();
                // on changing the page
                // make respected tab selected
                //tabLayout.set
                //actionBar.setSelectedNavigationItem(position);
                //Toast.makeText(Telecaller_Followup_Activity.this, "Position"+position , Toast.LENGTH_SHORT).show();
                //tabLayout.set
                //FragmentTransaction ft = getFragmentManager().beginTransaction();
                //ft.detach(TelecallerOne).attach(YourFragment.this).commit();


            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });*/


        /*  mViewPager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });*/

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //mViewPager.setCurrentItem(tab.getPosition());

                    if (tab.getPosition() == 0) {
                        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Telecaller_Followup_Activity.this);
                        Intent i = new Intent("TAG_REFRESH2");
                        lbm.sendBroadcast(i);

                    }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    public void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.icons_layout, fragment);
        transaction.commit();
    }//mViewPager.OnPageChangeListener(new ViewPager.OnPageChangeListener);
    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {

    }

  private class CustomPagerAdapter extends FragmentStatePagerAdapter {
      int tabCount;
      private String[] tabTitles = new String[]{"Follow Up"};
      private CustomPagerAdapter(FragmentManager fm, int tabCount) {
          super(fm);
          this.tabCount= tabCount;
      }


      // overriding getPageTitle()
      @Override
      public CharSequence getPageTitle(int position) {
          return tabTitles[position];
      }

      @Override
      public Fragment getItem(int position) {
             return new TelecallerTwo();
      }

      @Override
      public int getCount() {
          return tabCount;

      }


  /*  @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "General";
            case 1:
                return "Follow Up";

        }

        return null;
    }*/


  }
      /*  class CustomPagerAdapter extends FragmentPagerAdapter {

            Context mContext;

            public CustomPagerAdapter(FragmentManager fm, Context context) {
                super(fm);
                mContext = context;
            }

            @Override
            public Fragment getItem(int position) {

                // Create fragment object
                Fragment fragment = new DemoFragment();

                // Attach some data to the fragment
                // that we'll use to populate our fragment layouts
                Bundle args = new Bundle();
                args.putInt("page_position", position + 1);

                // Set the arguments on the fragment
                // that will be fetched in the
                // DemoFragment@onCreateView
                fragment.setArguments(args);

                return fragment;
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return "Page " + (position + 1);
            }
        }*/

   /* private class DemoFragment extends Fragment {
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                // Inflate the layout resource that'll be returned
                View rootView = inflater.inflate(R.layout.telecaller1, container, false);

                // Get the arguments that was supplied when
                // the fragment was instantiated in the
                // CustomPagerAdapter
                Bundle args = getArguments();
                ((TextView) rootView.findViewById(R.id.textView)).setText("Page " + args.getInt("page_position"));

                return rootView;
            }
        }*/




    }