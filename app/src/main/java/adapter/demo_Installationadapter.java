package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.brainmagic.toyumwater.R;

import java.util.List;

import dealer.Demo_Update_Activity;
import model.Dealer.demopendinginstalls.Datum;


/**
 * Created by Systems02 on 05-Jul-17.
 */

public class demo_Installationadapter extends RecyclerView.Adapter<demo_Installationadapter.MyViewHolder> {

    private List<Datum> InstallationList;
    private Context context;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView SNo,Text1,Text2,Text3;
        private Button Update;


        private MyViewHolder(View itemView) {
            super(itemView);

            this.SNo = (TextView) itemView.findViewById(R.id.sno);
            this.Text1 = (TextView) itemView.findViewById(R.id.text1);
            this.Text2 = (TextView) itemView.findViewById(R.id.text2);
            this.Text3 = (TextView) itemView.findViewById(R.id.text3);
            this.Update = (Button) itemView.findViewById(R.id.update);

        }
    }

    public demo_Installationadapter(Context context, List<Datum> data) {
        this.context = context;
        this.InstallationList = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_installation, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        TextView SNo = holder.SNo;
        TextView Text1 = holder.Text1;
        TextView Text2 = holder.Text2;
        TextView Text3 = holder.Text3;
        Button Update = holder.Update;


        SNo.setText(String.format("%s",(listPosition+1)));
        Text1.setText(String.format("%s", InstallationList.get(listPosition).getCustomerName()+"\n"+InstallationList.get(listPosition).getCustomerCode()));
        Text2.setText(InstallationList.get(listPosition).getMobile());
        //Text3.setText(InstallationList.get(listPosition).getAddress());
        Text3.setText(String.format("%s", InstallationList.get(listPosition).getDoorNo() + ", " + InstallationList.get(listPosition).getStreet() + ", " + InstallationList.get(listPosition).getArea()));
        //Text4.setText("Update");
        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //InstallationUpdate Updation = new InstallationUpdate(context,InstallationList, listPosition);
                //new InstallationUpdate(context,InstallationList, listPosition ).showupdatebox();
//                Updation.showupdatebox();
/*
                 AlertDialog alertDialog = new AlertDialog.Builder(
                        context).create();
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.installation_update, null);


                alertDialog.setView(dialogView);
                alertDialog.show();
            */
                context.startActivity(new Intent(context, Demo_Update_Activity.class)
                        .putExtra("id",InstallationList.get(listPosition).getId())
                        .putExtra("code",InstallationList.get(listPosition).getCustomerCode())
                        .putExtra("name",InstallationList.get(listPosition).getCustomerName())
                        .putExtra("cans",InstallationList.get(listPosition).getCansLimit())
                );



            }
        });


    }

    @Override
    public int getItemCount() {
        return InstallationList.size();
    }
}

/*
public class Installationadapter extends RecyclerView.Adapter<Installationadapter.MyViewHolder> {

    private List<Datum> InstallationList;
    private Context context;
    int sum;

    //private static final int FOOTER_VIEW = 1;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Serial,PartNo,PartName,MRP,Incentive,date,status,SNo,Amount;


        public MyViewHolder(View view) {
            super(view);
            SNo = (TextView) view.findViewById(R.id.sno);
            Serial = (TextView) view.findViewById(R.id.text1);
            PartNo = (TextView) view.findViewById(R.id.text2);
            PartName = (TextView) view.findViewById(R.id.text3);
            MRP = (TextView) view.findViewById(R.id.text4);
            */
/*Incentive = (TextView) view.findViewById(R.id.text5);
            date = (TextView) view.findViewById(R.id.text6);
            status = (TextView) view.findViewById(R.id.text7);
            Amount  = (TextView) view.findViewById(R.id.amount);*//*

        }
        public void bindView(int position) {
            // bindView() method to implement actions
        }

    }


    public class NormalViewHolder extends MyViewHolder {
        public NormalViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the normal items
                }
            });
        }
    }



   */
/* public class FooterViewHolder extends MyViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the item
                }
            });
        }
    }
*//*




    public Installationadapter(Context context, List<Datum> InstallationList) {
        this.context = context;
        this.InstallationList = InstallationList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_installation, parent, false);

        View v;

      */
/*  if (viewType == FOOTER_VIEW) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footerview_asc_details, parent, false);

            return new FooterViewHolder(v);
        }
*//*

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_installation, parent, false);

        return new NormalViewHolder(v);


        //return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {



        try {
            if (holder instanceof NormalViewHolder) {
                NormalViewHolder vh = (NormalViewHolder) holder;
                vh.bindView(position);
                final Datum installationList = InstallationList.get(position);
        holder.SNo.setText(String.format("%s",(position+1)));
        holder.Serial.setText(installationList.getCustomerName());
        holder.PartNo.setText(installationList.getMobile());
        holder.PartName.setText(installationList.getAddress());
                //holder.PartNo.setText(String.format("%s",installationList.getMobile()));
        */
/*holder.MRP.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(aschistory.getMRP().toString())));
        holder.Incentive.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(aschistory.getIncentiveAmount().toString())));
        holder.date.setText(Trim(aschistory.getScannedDate()));*//*

       */
/* if(aschistory.getIsRedeemed())
        holder.status.setText(R.string.redem);*//*

            } */
/*else if (holder instanceof FooterViewHolder) {
                FooterViewHolder vh = (FooterViewHolder) holder;
                vh.bindView(position);
                totalamout();

                holder.Amount.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(String.valueOf(sum))));

            }*//*

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

   */
/* @Override
    public int getItemCount() {
        return ASCHistoryList.size();
    }*//*


   */
/* @Override
    public int getItemViewType(int position) {
        if (position == ASCHistoryList.size()) {
            // This is where we'll add footer.
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }*//*

*/
/*
    @Override
    public int getItemCount() {
        if (InstallationList == null) {
            return 0;
        }

        if (InstallationList.size() == 0) {
            //Return 1 here to show nothing
            return 1;
        }

        // Add extra view to show the footer view
        return InstallationList.size() + 1;
    }

*//*


    @Override
    public int getItemCount() {
        return 0;
    }

*/
/* private void totalamout() {

        for (int j = 0; j < ASCHistoryList.size(); j++) {
            final Datum aschistory = ASCHistoryList.get(j);
           // Datum item = getItem(j);
            sum += aschistory.getIncentiveAmount().intValue();
        }
        //holder.Amount.setText(String.format("%s", context.getString(R.string.RS)+" "+sum));

    }*//*

   */
/* private String Trim(String s) {
        return s.substring(0, Math.min(s.length(), 10));
    }*//*

  */
/* private String Trim(String s) {
       if(!Objects.equals(s, null))
           if(!Objects.equals(s, ""))
               return s.substring(0, Math.min(s.length(), 10));
           else
               return s;
       return s;
   }*//*



}



*/


