package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.inputmethodservice.Keyboard;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.toyumwater.R;

import java.util.Calendar;
import java.util.List;


import alertbox.AlertDialogue;
import fragments.TelecallerOne;
import model.Telecaller.GeneralCustomerList.Datum;
import model.Telecaller.UpdateTelecaller.UpdateCustomerTelecaller;
import retroclient.RetroClient;
import toaster.Toasts;
import listener.CustomItemClickListener;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import apiservice.APIService;

/**
 * Created by Systems02 on 05-Jul-17.
 */

public class GeneralListadapter extends RecyclerView.Adapter<GeneralListadapter.MyViewHolder> {

    private List<Datum> HistoryList;
    private Context context;
    private AlertDialog alertDialog;
    private CustomItemClickListener listener;
    private TelecallerOne fragmant;
    private int PHONE_PERMISSION_CODE = 03;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView SNo, Text1, Text2;
        private Button Update;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.SNo = (TextView) itemView.findViewById(R.id.sno);
            this.Text1 = (TextView) itemView.findViewById(R.id.text1);
            this.Text2 = (TextView) itemView.findViewById(R.id.text2);
            this.Update = (Button) itemView.findViewById(R.id.update);

        }
    }

    public GeneralListadapter(Context context, List<Datum> data, TelecallerOne fragment) {
        this.context = context;
        this.HistoryList = data;
        this.fragmant = fragment;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_general_list, parent, false);

        final MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView SNo = holder.SNo;
        TextView Text1 = holder.Text1;
        TextView Text2 = holder.Text2;
        Button Update = holder.Update;

        SNo.setText(String.format("%s", (listPosition + 1)));
        if (String.format("%s", (HistoryList.get(listPosition).getName())).equals("null")) {
            Text1.setText("");
        } else {
            Text1.setText(String.format("%s", (HistoryList.get(listPosition).getName())));
        }
        Text2.setText(String.format("%s", (HistoryList.get(listPosition).getMobile())));

        //Update.setText(String.format("%s",(HistoryList.get(listPosition).getMobile())));
        Text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + HistoryList.get(listPosition).getMobile())));
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, android.Manifest.permission.CALL_PHONE)) {
                            Toast.makeText(context, "App requires Phone Call permission.\nPlease allow that in the device settings.", Toast.LENGTH_LONG).show();
                        }
                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, PHONE_PERMISSION_CODE);
                    }
                } else {
                    context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + HistoryList.get(listPosition).getMobile())));
                }

            /*    Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + HistoryList.get(listPosition).getMobile()));
                // callIntent.setData(Uri.parse(MobileList.get(position)));
                //callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        *//* if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                             // TODO: Consider calling
                                             //    ActivityCompat#requestPermissions
                                             // here to request the missing permissions, and then overriding
                                             //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                             //                                          int[] grantResults)
                                             // to handle the case where the user grants the permission. See the documentation
                                             // for ActivityCompat#requestPermissions for more details.
                                             return;
                                         }*//*
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(callIntent);*/
            }
        });

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(context).create();
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.import_update, null);
                alertDialog.setView(dialogView);
                final NetworkConnection network = new NetworkConnection(context);
                final Toasts toaster = new Toasts(context);
                final AlertDialogue alertDialogue = new AlertDialogue(context);
                //DatePickerDialog.OnDateSetListener date = null;

                //final Calendar myCalendar = Calendar.getInstance();
               /* ArrayList<String> orderTypeList = new ArrayList<String>();
                orderTypeList.add("Select Status");*/
                final String[] Status = new String[1];
                final String[] Reason = new String[1];
                final TextView Remarks = (TextView) dialogView.findViewById(R.id.remarks);
                final EditText CustomerName = (EditText) dialogView.findViewById(R.id.customer_name);
                TextView CustomerMobile = (TextView) dialogView.findViewById(R.id.customer_mobile);
                final EditText Appointment = (EditText) dialogView.findViewById(R.id.appointment);
                final Spinner StatusSpinner = (Spinner) dialogView.findViewById(R.id.statusspinner);

                final Spinner ReasonSpinner = (Spinner) dialogView.findViewById(R.id.reasonspinner);
                final TableRow ReasonRow = (TableRow) dialogView.findViewById(R.id.l_callback);
                final TableRow DateRow = (TableRow) dialogView.findViewById(R.id.app_row);

                Button Cancel = (Button) dialogView.findViewById(R.id.cancel);
                Button Update = (Button) dialogView.findViewById(R.id.update);
                final String[] S_Date = new String[1];

                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(dialogView.getContext(), android.R.layout.simple_spinner_item,orderTypeList);
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.country_arrays, R.layout.spinner_item);
                //spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                StatusSpinner.setAdapter(adapter);

                ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(context, R.array.callback_reason, R.layout.spinner_item);
                //spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                ReasonSpinner.setAdapter(adapter2);

                if (String.format("%s", (HistoryList.get(listPosition).getName())).equals("null")) {
                    CustomerName.setText("");
                } else {
                    CustomerName.setText(String.format("%s", HistoryList.get(listPosition).getName()));
                }
                CustomerMobile.setText(String.format("%s", HistoryList.get(listPosition).getMobile()));

                Appointment.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View v) {
                                                       //Calendar now = Calendar.getInstance();
                                                       final Calendar c = Calendar.getInstance();

                                                       DatePickerDialog dpd = new DatePickerDialog(context,
                                                               new DatePickerDialog.OnDateSetListener() {

                                                                   @Override
                                                                   public void onDateSet(DatePicker view, int year,
                                                                                         int monthOfYear, int dayOfMonth) {
                                                                       Appointment.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                                                       S_Date[0] = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                                                   }
                                                               }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                                                       dpd.show();
                                                   }
                                               }

                );

                StatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (parent.getSelectedItem().toString().equals("Call Back")) {
                            ReasonRow.setVisibility(View.VISIBLE);
                            DateRow.setVisibility(View.GONE);
                        } else if (parent.getSelectedItem().toString().equals("Confirmed Order")) {
                            ReasonRow.setVisibility(View.GONE);
                            DateRow.setVisibility(View.GONE);
                        }
                        //Appointment
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                ReasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (parent.getSelectedItem().toString().equals("Appointment"))
                            DateRow.setVisibility(View.VISIBLE);
                        else
                            DateRow.setVisibility(View.GONE);
                        //Appointment
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                Cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                Update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Status[0] = StatusSpinner.getSelectedItem().toString();
                        if (Status[0].equals("Select Status")) {
                            toaster.ShowErrorToast("Kindly select Status!");
                        } else if (Status[0].equals("Call Back")) {
                            Reason[0] = ReasonSpinner.getSelectedItem().toString();
                            if (Reason[0].equals("Select Reason")) {
                                toaster.ShowErrorToast("Kindly select Reason!");
                            } else if (Reason[0].equals("Appointment")) {
                                if (Appointment.getText().toString().equals(""))
                                    toaster.ShowErrorToast("Kindly select Date!");
                                else if (network.CheckInternet()) {
                                    UpdateInstallation();
                                } else
                                    alertDialogue.showAlertbox(context.getString(R.string.no_network));
                            } else if (network.CheckInternet()) {
                                UpdateInstallation();
                            } else
                                alertDialogue.showAlertbox(context.getString(R.string.no_network));

                        } else if (network.CheckInternet())

                            UpdateInstallation();
                        else {
                            alertDialogue.showAlertbox(context.getString(R.string.no_network));
                        }

                    }



                    private void UpdateInstallation() {
                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("Loading...");
                        progressDialog.show();
                        APIService service = RetroClient.getApiService();

                        Call<UpdateCustomerTelecaller> call = service.UPDATE_CUSTOMER_TELECALLER_CALL(
                                Integer.parseInt(HistoryList.get(listPosition).getId().toString()),
                                CustomerName.getText().toString(),
                                HistoryList.get(listPosition).getEmail(),
                                Status[0],
                                S_Date[0],
                                Reason[0],
                                Remarks.getText().toString()
                        );

                        call.enqueue(new Callback<UpdateCustomerTelecaller>() {
                            @Override
                            public void onResponse(Call<UpdateCustomerTelecaller> call, Response<UpdateCustomerTelecaller> response) {
                                progressDialog.dismiss();
                                switch (response.body().getResult()) {
                                    case "Success": {
                                        alertDialogue.showAlertbox("Customer Information updated Successfully");
                                        alertDialogue.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                alertDialog.dismiss();
                                                // ((Activity) context).recreate();
                                                fragmant.Get_GeneralList();
                                            }
                                        });
                                        break;
                                    }
                                    case "Failure": {
                                        alertDialogue.showAlertbox("Updated failed, Please try again !");
                                        break;
                                    }
                                    default: {
                                        AlertDialogue alertnew = new AlertDialogue(context);
                                        alertnew.showAlertbox(context.getString(R.string.connection_slow));
                                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {

                                            }
                                        });
                                        break;
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdateCustomerTelecaller> call, Throwable t) {
                                alertDialog.dismiss();
                                progressDialog.dismiss();
                                alertDialogue.showAlertbox(context.getString(R.string.connection_slow));

                            }
                        });
                    }
                });

                alertDialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return HistoryList.size();
    }


}

/*
public class Installationadapter extends RecyclerView.Adapter<Installationadapter.MyViewHolder> {

    private List<Datum> HistoryList;
    private Context context;
    int sum;

    //private static final int FOOTER_VIEW = 1;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Serial,PartNo,PartName,MRP,Incentive,date,status,SNo,Amount;


        public MyViewHolder(View view) {
            super(view);
            SNo = (TextView) view.findViewById(R.id.sno);
            Serial = (TextView) view.findViewById(R.id.text1);
            PartNo = (TextView) view.findViewById(R.id.text2);
            PartName = (TextView) view.findViewById(R.id.text3);
            MRP = (TextView) view.findViewById(R.id.text4);
            */
/*Incentive = (TextView) view.findViewById(R.id.text5);
            date = (TextView) view.findViewById(R.id.text6);
            status = (TextView) view.findViewById(R.id.text7);
            Amount  = (TextView) view.findViewById(R.id.amount);*//*

        }
        public void bindView(int position) {
            // bindView() method to implement actions
        }

    }


    public class NormalViewHolder extends MyViewHolder {
        public NormalViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the normal items
                }
            });
        }
    }



   */
/* public class FooterViewHolder extends MyViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the item
                }
            });
        }
    }
*//*




    public Installationadapter(Context context, List<Datum> HistoryList) {
        this.context = context;
        this.HistoryList = HistoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_installation, parent, false);

        View v;

      */
/*  if (viewType == FOOTER_VIEW) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footerview_asc_details, parent, false);

            return new FooterViewHolder(v);
        }
*//*

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_installation, parent, false);

        return new NormalViewHolder(v);


        //return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {



        try {
            if (holder instanceof NormalViewHolder) {
                NormalViewHolder vh = (NormalViewHolder) holder;
                vh.bindView(position);
                final Datum installationList = HistoryList.get(position);
        holder.SNo.setText(String.format("%s",(position+1)));
        holder.Serial.setText(installationList.getCustomerName());
        holder.PartNo.setText(installationList.getMobile());
        holder.PartName.setText(installationList.getAddress());
                //holder.PartNo.setText(String.format("%s",installationList.getMobile()));
        */
/*holder.MRP.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(aschistory.getMRP().toString())));
        holder.Incentive.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(aschistory.getIncentiveAmount().toString())));
        holder.date.setText(Trim(aschistory.getScannedDate()));*//*

 */
/* if(aschistory.getIsRedeemed())
        holder.status.setText(R.string.redem);*//*

            } */
/*else if (holder instanceof FooterViewHolder) {
                FooterViewHolder vh = (FooterViewHolder) holder;
                vh.bindView(position);
                totalamout();

                holder.Amount.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(String.valueOf(sum))));

            }*//*

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

   */
/* @Override
    public int getItemCount() {
        return ASCHistoryList.size();
    }*//*


 */
/* @Override
    public int getItemViewType(int position) {
        if (position == ASCHistoryList.size()) {
            // This is where we'll add footer.
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }*//*

 */
/*
    @Override
    public int getItemCount() {
        if (HistoryList == null) {
            return 0;
        }

        if (HistoryList.size() == 0) {
            //Return 1 here to show nothing
            return 1;
        }

        // Add extra view to show the footer view
        return HistoryList.size() + 1;
    }

*//*


    @Override
    public int getItemCount() {
        return 0;
    }

*/
/* private void totalamout() {

        for (int j = 0; j < ASCHistoryList.size(); j++) {
            final Datum aschistory = ASCHistoryList.get(j);
           // Datum item = getItem(j);
            sum += aschistory.getIncentiveAmount().intValue();
        }
        //holder.Amount.setText(String.format("%s", context.getString(R.string.RS)+" "+sum));

    }*//*

 */
/* private String Trim(String s) {
        return s.substring(0, Math.min(s.length(), 10));
    }*//*

 */
/*



}



*/


