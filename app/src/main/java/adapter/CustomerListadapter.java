package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.toyumwater.R;

import java.util.ArrayList;
import java.util.List;

import model.Dealer.CustomerList.Datum;
import model.customerlist.Cu;
import model.customerlist.Rg;


/**
 * Created by Systems02 on 05-Jul-17.
 */

public class CustomerListadapter extends RecyclerView.Adapter<CustomerListadapter.MyViewHolder> {

    private List<Cu> HistoryList;
    private List<Rg> rgList;
    private Context context;
    private AlertDialog alertDialog;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView SNo, Text1, Text2, Text3, Text4, Text5;
        private Button More;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.SNo = (TextView) itemView.findViewById(R.id.sno);
            this.Text1 = (TextView) itemView.findViewById(R.id.text1);
            this.Text2 = (TextView) itemView.findViewById(R.id.text2);
            this.Text3 = (TextView) itemView.findViewById(R.id.text3);
            this.Text4 = (TextView) itemView.findViewById(R.id.text4);
            this.Text5 = (TextView) itemView.findViewById(R.id.text5);
            this.More = (Button) itemView.findViewById(R.id.more);
            //this.Update = (ImageView) itemView.findViewById(R.id.text4);

        }
    }

//    public CustomerListadapter(Context context, List<Datum> data) {
//        this.context = context;
//        this.HistoryList = data;
//    }

    public CustomerListadapter(Context context, List<Cu> data, List<Rg> rgs) {
        this.context = context;
        this.rgList = rgs;
        this.HistoryList = data;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_customer_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        TextView SNo = holder.SNo;
        TextView Text1 = holder.Text1;
        TextView Text2 = holder.Text2;
        TextView Text3 = holder.Text3;
        TextView Text4 = holder.Text4;
        TextView Text5 = holder.Text5;
        Button More = holder.More;


        SNo.setText(String.format("%s", (listPosition + 1)));
//        Text1.setText(String.format("%s", HistoryList.get(listPosition).getCustomerName() + "\n" + HistoryList.get(listPosition).getCustomerCode()));
        Text1.setText(String.format("%s", HistoryList.get(listPosition).getCustomerName()));
        Text2.setText(HistoryList.get(listPosition).getAddress());

        final SpannableString content = new SpannableString(HistoryList.get(listPosition).getMobile());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

        Text3.setText(content);
        Text3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel: " +HistoryList.get(listPosition).getMobile()));
                context.startActivity(callIntent);
            }
        });

        Text4.setText(String.format("%s", HistoryList.get(listPosition).getCansLimit()));
        Text5.setText(String.format("%s", (context.getString(R.string.RS) + " " + HistoryList.get(listPosition).getDepositAmountPaid() + ".00")));



        Text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String map = "http://maps.google.co.in/maps?q=" + HistoryList.get(listPosition).getLatitude() + "," + HistoryList.get(listPosition).getLangitude();
                Log.v("mapURL", map);
                Intent gotomap = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                context.startActivity(gotomap);
            }
        });
        More.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(context).create();
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.more_details, null);
                alertDialog.setView(dialogView);

                ImageView Back = (ImageView) dialogView.findViewById(R.id.back);
                TextView Name = (TextView) dialogView.findViewById(R.id.name);
                TextView Code = (TextView) dialogView.findViewById(R.id.code);
                TextView Mobile = (TextView) dialogView.findViewById(R.id.mobile);
                TextView Email = (TextView) dialogView.findViewById(R.id.email);
                TextView CansLimit = (TextView) dialogView.findViewById(R.id.canslimit);
                TextView Deposit = (TextView) dialogView.findViewById(R.id.deposit);
                TextView DepositPaid = (TextView) dialogView.findViewById(R.id.depositpaid);
                TextView QRcode = (TextView) dialogView.findViewById(R.id.qrcode);
                TextView Address = (TextView) dialogView.findViewById(R.id.address);
                TextView DoorNo = (TextView) dialogView.findViewById(R.id.door);
                TextView Street = (TextView) dialogView.findViewById(R.id.street);
                TextView Area = (TextView) dialogView.findViewById(R.id.area);
                TextView Landmark = (TextView) dialogView.findViewById(R.id.landmark);
                TextView Latitude = (TextView) dialogView.findViewById(R.id.latitude);
                TextView Longitude = (TextView) dialogView.findViewById(R.id.longitude);


                Name.setText(String.format("%s", HistoryList.get(listPosition).getCustomerName()));
                Code.setText(String.format("%s", HistoryList.get(listPosition).getCustomerCode()));
                SpannableString content1 = new SpannableString(String.format("%s", HistoryList.get(listPosition).getMobile()));
                content1.setSpan(new UnderlineSpan(), 0, content.length(), 0);

                Mobile.setText(content1);
                Mobile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callintent = new Intent(Intent.ACTION_DIAL);
                        callintent.setData(Uri.parse("tel: " +HistoryList.get(listPosition).getMobile()));
                        context.startActivity(callintent);
                    }
                });

//                Email.setText(String.format("%s", HistoryList.get(listPosition).getEmail()));
                Email.setText(String.format("%s", rgList.get(listPosition).getEmail()));
                CansLimit.setText(String.format("%s", HistoryList.get(listPosition).getCansLimit()));
                Deposit.setText(String.format("%s", context.getString(R.string.RS) + " " + HistoryList.get(listPosition).getDepositAmount() + "0"));
                DepositPaid.setText(String.format("%s", context.getString(R.string.RS) + " " + HistoryList.get(listPosition).getDepositAmountPaid() + "0"));
                QRcode.setText(String.format("%s", HistoryList.get(listPosition).getQRCode()));
                Address.setText(String.format("%s", HistoryList.get(listPosition).getAddress()));
                DoorNo.setText(String.format("%s", HistoryList.get(listPosition).getDoorNo()));
                Street.setText(String.format("%s", HistoryList.get(listPosition).getStreet()));
                Area.setText(String.format("%s", HistoryList.get(listPosition).getArea()));
                Landmark.setText(String.format("%s", HistoryList.get(listPosition).getLandmark()));
                Latitude.setText(String.format("%s", HistoryList.get(listPosition).getLatitude()));
                Longitude.setText(String.format("%s", HistoryList.get(listPosition).getLangitude()));

                Back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });


                alertDialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return HistoryList.size();
    }
}

/*
public class Installationadapter extends RecyclerView.Adapter<Installationadapter.MyViewHolder> {

    private List<Datum> HistoryList;
    private Context context;
    int sum;

    //private static final int FOOTER_VIEW = 1;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Serial,PartNo,PartName,MRP,Incentive,date,status,SNo,Amount;


        public MyViewHolder(View view) {
            super(view);
            SNo = (TextView) view.findViewById(R.id.sno);
            Serial = (TextView) view.findViewById(R.id.text1);
            PartNo = (TextView) view.findViewById(R.id.text2);
            PartName = (TextView) view.findViewById(R.id.text3);
            MRP = (TextView) view.findViewById(R.id.text4);
            */
/*Incentive = (TextView) view.findViewById(R.id.text5);
            date = (TextView) view.findViewById(R.id.text6);
            status = (TextView) view.findViewById(R.id.text7);
            Amount  = (TextView) view.findViewById(R.id.amount);*//*

        }
        public void bindView(int position) {
            // bindView() method to implement actions
        }

    }


    public class NormalViewHolder extends MyViewHolder {
        public NormalViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the normal items
                }
            });
        }
    }



   */
/* public class FooterViewHolder extends MyViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the item
                }
            });
        }
    }
*//*




    public Installationadapter(Context context, List<Datum> HistoryList) {
        this.context = context;
        this.HistoryList = HistoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_installation, parent, false);

        View v;

      */
/*  if (viewType == FOOTER_VIEW) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footerview_asc_details, parent, false);

            return new FooterViewHolder(v);
        }
*//*

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_installation, parent, false);

        return new NormalViewHolder(v);


        //return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {



        try {
            if (holder instanceof NormalViewHolder) {
                NormalViewHolder vh = (NormalViewHolder) holder;
                vh.bindView(position);
                final Datum installationList = HistoryList.get(position);
        holder.SNo.setText(String.format("%s",(position+1)));
        holder.Serial.setText(installationList.getCustomerName());
        holder.PartNo.setText(installationList.getMobile());
        holder.PartName.setText(installationList.getAddress());
                //holder.PartNo.setText(String.format("%s",installationList.getMobile()));
        */
/*holder.MRP.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(aschistory.getMRP().toString())));
        holder.Incentive.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(aschistory.getIncentiveAmount().toString())));
        holder.date.setText(Trim(aschistory.getScannedDate()));*//*

 */
/* if(aschistory.getIsRedeemed())
        holder.status.setText(R.string.redem);*//*

            } */
/*else if (holder instanceof FooterViewHolder) {
                FooterViewHolder vh = (FooterViewHolder) holder;
                vh.bindView(position);
                totalamout();

                holder.Amount.setText(String.format(context.getString(R.string.RS)+" "+"%.2f",Double.parseDouble(String.valueOf(sum))));

            }*//*

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

   */
/* @Override
    public int getItemCount() {
        return ASCHistoryList.size();
    }*//*


 */
/* @Override
    public int getItemViewType(int position) {
        if (position == ASCHistoryList.size()) {
            // This is where we'll add footer.
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }*//*

 */
/*
    @Override
    public int getItemCount() {
        if (HistoryList == null) {
            return 0;
        }

        if (HistoryList.size() == 0) {
            //Return 1 here to show nothing
            return 1;
        }

        // Add extra view to show the footer view
        return HistoryList.size() + 1;
    }

*//*


    @Override
    public int getItemCount() {
        return 0;
    }

*/
/* private void totalamout() {

        for (int j = 0; j < ASCHistoryList.size(); j++) {
            final Datum aschistory = ASCHistoryList.get(j);
           // Datum item = getItem(j);
            sum += aschistory.getIncentiveAmount().intValue();
        }
        //holder.Amount.setText(String.format("%s", context.getString(R.string.RS)+" "+sum));

    }*//*

 */
/* private String Trim(String s) {
        return s.substring(0, Math.min(s.length(), 10));
    }*//*

 */
/* private String Trim(String s) {
       if(!Objects.equals(s, null))
           if(!Objects.equals(s, ""))
               return s.substring(0, Math.min(s.length(), 10));
           else
               return s;
       return s;
   }*//*



}



*/


