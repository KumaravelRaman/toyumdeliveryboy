
package model.customerlist;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("cu")
    private List<Cu> mCu;
    @SerializedName("rg")
    private List<Rg> mRg;

    public List<Cu> getCu() {
        return mCu;
    }

    public void setCu(List<Cu> cu) {
        mCu = cu;
    }

    public List<Rg> getRg() {
        return mRg;
    }

    public void setRg(List<Rg> rg) {
        mRg = rg;
    }

}
