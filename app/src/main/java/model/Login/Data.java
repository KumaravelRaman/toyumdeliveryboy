
package model.Login;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Area")
    private String mArea;
    @SerializedName("CansLimit")
    private Long mCansLimit;
    @SerializedName("Center")
    private String mCenter;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Comments")
    private String mComments;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerStatus")
    private String mCustomerStatus;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("CustomerZone")
    private String mCustomerZone;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DepositAmount")
    private Long mDepositAmount;
    @SerializedName("DepositAmountPaid")
    private Long mDepositAmountPaid;
    @SerializedName("DepositPaidDate")
    private String mDepositPaidDate;
    @SerializedName("DoorNo")
    private String mDoorNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Id")
    private Long mId;

    public Long getmTeleId() {
        return mTeleId;
    }

    public void setmTeleId(Long mTeleId) {
        this.mTeleId = mTeleId;
    }

    @SerializedName("id")
    private Long mTeleId;
    @SerializedName("InstalledBy")
    private String mInstalledBy;
    @SerializedName("InstalledDate")
    private String mInstalledDate;
    @SerializedName("Landmark")
    private String mLandmark;
    @SerializedName("Langitude")
    private Double mLangitude;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("PayType")
    private String mPayType;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("QRCode")
    private String mQRCode;
    @SerializedName("ReasonForInactive")
    private String mReasonForInactive;
    @SerializedName("SalesExecutive")
    private String mSalesExecutive;
    @SerializedName("SplRate")
    private Long mSplRate;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("UpdatedDate")
    private String mUpdatedDate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("UserType")
    private String mUserType;
    @SerializedName("Telecode")
    private String mTelecode;


    //Name
    @SerializedName("Name")
    private String mName;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String Area) {
        mArea = Area;
    }

    public Long getCansLimit() {
        return mCansLimit;
    }

    public void setCansLimit(Long CansLimit) {
        mCansLimit = CansLimit;
    }

    public String getCenter() {
        return mCenter;
    }

    public void setCenter(String Center) {
        mCenter = Center;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getComments() {
        return mComments;
    }

    public void setComments(String Comments) {
        mComments = Comments;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public String getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(String CustomerStatus) {
        mCustomerStatus = CustomerStatus;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String CustomerType) {
        mCustomerType = CustomerType;
    }

    public String getCustomerZone() {
        return mCustomerZone;
    }

    public void setCustomerZone(String CustomerZone) {
        mCustomerZone = CustomerZone;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String DealerCode) {
        mDealerCode = DealerCode;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String DealerName) {
        mDealerName = DealerName;
    }

    public Long getDepositAmount() {
        return mDepositAmount;
    }

    public void setDepositAmount(Long DepositAmount) {
        mDepositAmount = DepositAmount;
    }

    public Long getDepositAmountPaid() {
        return mDepositAmountPaid;
    }

    public void setDepositAmountPaid(Long DepositAmountPaid) {
        mDepositAmountPaid = DepositAmountPaid;
    }

    public String getDepositPaidDate() {
        return mDepositPaidDate;
    }

    public void setDepositPaidDate(String DepositPaidDate) {
        mDepositPaidDate = DepositPaidDate;
    }

    public String getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(String DoorNo) {
        mDoorNo = DoorNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public String getInstalledBy() {
        return mInstalledBy;
    }

    public void setInstalledBy(String InstalledBy) {
        mInstalledBy = InstalledBy;
    }

    public String getInstalledDate() {
        return mInstalledDate;
    }

    public void setInstalledDate(String InstalledDate) {
        mInstalledDate = InstalledDate;
    }

    public String getLandmark() {
        return mLandmark;
    }

    public void setLandmark(String Landmark) {
        mLandmark = Landmark;
    }

    public Double getLangitude() {
        return mLangitude;
    }

    public void setLangitude(Double Langitude) {
        mLangitude = Langitude;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double Latitude) {
        mLatitude = Latitude;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String Password) {
        mPassword = Password;
    }

    public String getPayType() {
        return mPayType;
    }

    public void setPayType(String PayType) {
        mPayType = PayType;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String Pincode) {
        mPincode = Pincode;
    }

    public String getQRCode() {
        return mQRCode;
    }

    public void setQRCode(String QRCode) {
        mQRCode = QRCode;
    }

    public String getReasonForInactive() {
        return mReasonForInactive;
    }

    public void setReasonForInactive(String ReasonForInactive) {
        mReasonForInactive = ReasonForInactive;
    }

    public String getSalesExecutive() {
        return mSalesExecutive;
    }

    public void setSalesExecutive(String SalesExecutive) {
        mSalesExecutive = SalesExecutive;
    }

    public Long getSplRate() {
        return mSplRate;
    }

    public void setSplRate(Long SplRate) {
        mSplRate = SplRate;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String Street) {
        mStreet = Street;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String UserName) {
        mUserName = UserName;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String UserType) {
        mUserType = UserType;
    }

    public String getTelecode() {
        return mTelecode;
    }

    public void setTelecode(String Telecode) {
        this.mTelecode = mTelecode;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        this.mName = Name;
    }
}