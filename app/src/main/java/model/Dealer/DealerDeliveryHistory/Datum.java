
package model.Dealer.DealerDeliveryHistory;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Amount")
    private String mAmount;
    @SerializedName("AmountFor")
    private String mAmountFor;
    @SerializedName("CallDate")
    private String mCallDate;
    @SerializedName("canPerCost")
    private Object mCanPerCost;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("CustId")
    private Long mCustId;
    @SerializedName("CustomerAddress")
    private String mCustomerAddress;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerMobile")
    private String mCustomerMobile;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DeliveryDate")
    private String mDeliveryDate;
    @SerializedName("DepositCost")
    private Object mDepositCost;
    @SerializedName("EmptyCans")
    private Long mEmptyCans;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Longitude")
    private Double mLongitude;
    @SerializedName("OrderFrom")
    private String mOrderFrom;
    @SerializedName("OrderNo")
    private String mOrderNo;
    @SerializedName("PaymentMode")
    private String mPaymentMode;
    @SerializedName("PaymentStatus")
    private String mPaymentStatus;
    @SerializedName("QRCode")
    private String mQRCode;
    @SerializedName("ReceiptNo")
    private String mReceiptNo;
    @SerializedName("ReceiptNoFormat")
    private Long mReceiptNoFormat;
    @SerializedName("RefillCans")
    private Long mRefillCans;
    @SerializedName("RefillCansDeliverd")
    private Long mRefillCansDeliverd;
    @SerializedName("RefillStatus")
    private String mRefillStatus;
    @SerializedName("Remarks")
    private String mRemarks;
    @SerializedName("SuppliedDate")
    private String mSuppliedDate;
    @SerializedName("UpdatedDate")
    private Object mUpdatedDate;
    @SerializedName("WhatsAppNo")
    private Object mWhatsAppNo;

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public String getAmountFor() {
        return mAmountFor;
    }

    public void setAmountFor(String amountFor) {
        mAmountFor = amountFor;
    }

    public String getCallDate() {
        return mCallDate;
    }

    public void setCallDate(String callDate) {
        mCallDate = callDate;
    }

    public Object getCanPerCost() {
        return mCanPerCost;
    }

    public void setCanPerCost(Object canPerCost) {
        mCanPerCost = canPerCost;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public Long getCustId() {
        return mCustId;
    }

    public void setCustId(Long custId) {
        mCustId = custId;
    }

    public String getCustomerAddress() {
        return mCustomerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        mCustomerAddress = customerAddress;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String customerCode) {
        mCustomerCode = customerCode;
    }

    public String getCustomerMobile() {
        return mCustomerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        mCustomerMobile = customerMobile;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String customerName) {
        mCustomerName = customerName;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String dealerCode) {
        mDealerCode = dealerCode;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String dealerName) {
        mDealerName = dealerName;
    }

    public String getDeliveryDate() {
        return mDeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        mDeliveryDate = deliveryDate;
    }

    public Object getDepositCost() {
        return mDepositCost;
    }

    public void setDepositCost(Object depositCost) {
        mDepositCost = depositCost;
    }

    public Long getEmptyCans() {
        return mEmptyCans;
    }

    public void setEmptyCans(Long emptyCans) {
        mEmptyCans = emptyCans;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        mLatitude = latitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double longitude) {
        mLongitude = longitude;
    }

    public String getOrderFrom() {
        return mOrderFrom;
    }

    public void setOrderFrom(String orderFrom) {
        mOrderFrom = orderFrom;
    }

    public String getOrderNo() {
        return mOrderNo;
    }

    public void setOrderNo(String orderNo) {
        mOrderNo = orderNo;
    }

    public String getPaymentMode() {
        return mPaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        mPaymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return mPaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        mPaymentStatus = paymentStatus;
    }

    public String getQRCode() {
        return mQRCode;
    }

    public void setQRCode(String qRCode) {
        mQRCode = qRCode;
    }

    public String getReceiptNo() {
        return mReceiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        mReceiptNo = receiptNo;
    }

    public Long getReceiptNoFormat() {
        return mReceiptNoFormat;
    }

    public void setReceiptNoFormat(Long receiptNoFormat) {
        mReceiptNoFormat = receiptNoFormat;
    }

    public Long getRefillCans() {
        return mRefillCans;
    }

    public void setRefillCans(Long refillCans) {
        mRefillCans = refillCans;
    }

    public Long getRefillCansDeliverd() {
        return mRefillCansDeliverd;
    }

    public void setRefillCansDeliverd(Long refillCansDeliverd) {
        mRefillCansDeliverd = refillCansDeliverd;
    }

    public String getRefillStatus() {
        return mRefillStatus;
    }

    public void setRefillStatus(String refillStatus) {
        mRefillStatus = refillStatus;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String remarks) {
        mRemarks = remarks;
    }

    public String getSuppliedDate() {
        return mSuppliedDate;
    }

    public void setSuppliedDate(String suppliedDate) {
        mSuppliedDate = suppliedDate;
    }

    public Object getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(Object updatedDate) {
        mUpdatedDate = updatedDate;
    }

    public Object getWhatsAppNo() {
        return mWhatsAppNo;
    }

    public void setWhatsAppNo(Object whatsAppNo) {
        mWhatsAppNo = whatsAppNo;
    }

}
