
package model.Dealer.InstallationHistory;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Area")
    private Object mArea;
    @SerializedName("BankName")
    private String mBankName;
    @SerializedName("CanCost")
    private Double mCanCost;
    @SerializedName("CanCover")
    private Boolean mCanCover;
    @SerializedName("canPerCost")
    private Double mCanPerCost;
    @SerializedName("CansLimit")
    private Long mCansLimit;
    @SerializedName("Center")
    private String mCenter;
    @SerializedName("ChequeDate")
    private String mChequeDate;
    @SerializedName("ChequeNumber")
    private String mChequeNumber;
    @SerializedName("Cid")
    private Long mCid;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Comments")
    private Object mComments;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerStatus")
    private String mCustomerStatus;
    @SerializedName("CustomerType")
    private Object mCustomerType;
    @SerializedName("CustomerZone")
    private String mCustomerZone;
    @SerializedName("DOB")
    private Object mDOB;
    @SerializedName("data")
    private Object mData;
    @SerializedName("Date")
    private Object mDate;
    @SerializedName("DateOfMarrige")
    private Object mDateOfMarrige;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DealerMobile")
    private Object mDealerMobile;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DepositAmount")
    private Double mDepositAmount;
    @SerializedName("DepositAmountPaid")
    private Double mDepositAmountPaid;
    @SerializedName("DepositCost")
    private Double mDepositCost;
    @SerializedName("DepositPaid")
    private String mDepositPaid;
    @SerializedName("DepositPaidDate")
    private String mDepositPaidDate;
    @SerializedName("DepositPayType")
    private Object mDepositPayType;
    @SerializedName("DoorNo")
    private Object mDoorNo;
    @SerializedName("Email")
    private Object mEmail;
    @SerializedName("Email2")
    private Object mEmail2;
    @SerializedName("flag")
    private Object mFlag;
    @SerializedName("FlatName")
    private Object mFlatName;
    @SerializedName("FlatNo")
    private Object mFlatNo;
    @SerializedName("Floor")
    private Object mFloor;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("InstallationBy")
    private String mInstallationBy;
    @SerializedName("InstalledBy")
    private String mInstalledBy;
    @SerializedName("InstalledDate")
    private String mInstalledDate;
    @SerializedName("Landmark")
    private String mLandmark;
    @SerializedName("Langitude")
    private Double mLangitude;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Mobile2")
    private Object mMobile2;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("PayType")
    private String mPayType;
    @SerializedName("Pid")
    private Long mPid;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("Pump")
    private Boolean mPump;
    @SerializedName("QRCode")
    private Object mQRCode;
    @SerializedName("ReasonForInactive")
    private String mReasonForInactive;
    @SerializedName("ReceiptNo")
    private String mReceiptNo;
    @SerializedName("ReceiptNoFormat")
    private Long mReceiptNoFormat;
    @SerializedName("ReferEmail")
    private Object mReferEmail;
    @SerializedName("ReferMobile")
    private Object mReferMobile;
    @SerializedName("ReferName")
    private Object mReferName;
    @SerializedName("ReferUsertype")
    private Object mReferUsertype;
    @SerializedName("regId")
    private Long mRegId;
    @SerializedName("SalesExecutive")
    private Object mSalesExecutive;
    @SerializedName("SplRate")
    private Object mSplRate;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private Object mStreet;
    @SerializedName("UpdatedDate")
    private Object mUpdatedDate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("UserType")
    private Object mUserType;
    @SerializedName("WhatsAppNo")
    private String mWhatsAppNo;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Object getArea() {
        return mArea;
    }

    public void setArea(Object area) {
        mArea = area;
    }

    public String getBankName() {
        return mBankName;
    }

    public void setBankName(String bankName) {
        mBankName = bankName;
    }

    public Double getCanCost() {
        return mCanCost;
    }

    public void setCanCost(Double canCost) {
        mCanCost = canCost;
    }

    public Boolean getCanCover() {
        return mCanCover;
    }

    public void setCanCover(Boolean canCover) {
        mCanCover = canCover;
    }

    public Double getCanPerCost() {
        return mCanPerCost;
    }

    public void setCanPerCost(Double canPerCost) {
        mCanPerCost = canPerCost;
    }

    public Long getCansLimit() {
        return mCansLimit;
    }

    public void setCansLimit(Long cansLimit) {
        mCansLimit = cansLimit;
    }

    public String getCenter() {
        return mCenter;
    }

    public void setCenter(String center) {
        mCenter = center;
    }

    public String getChequeDate() {
        return mChequeDate;
    }

    public void setChequeDate(String chequeDate) {
        mChequeDate = chequeDate;
    }

    public String getChequeNumber() {
        return mChequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        mChequeNumber = chequeNumber;
    }

    public Long getCid() {
        return mCid;
    }

    public void setCid(Long cid) {
        mCid = cid;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public Object getComments() {
        return mComments;
    }

    public void setComments(Object comments) {
        mComments = comments;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String customerCode) {
        mCustomerCode = customerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String customerName) {
        mCustomerName = customerName;
    }

    public String getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        mCustomerStatus = customerStatus;
    }

    public Object getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(Object customerType) {
        mCustomerType = customerType;
    }

    public String getCustomerZone() {
        return mCustomerZone;
    }

    public void setCustomerZone(String customerZone) {
        mCustomerZone = customerZone;
    }

    public Object getDOB() {
        return mDOB;
    }

    public void setDOB(Object dOB) {
        mDOB = dOB;
    }

    public Object getData() {
        return mData;
    }

    public void setData(Object data) {
        mData = data;
    }

    public Object getDate() {
        return mDate;
    }

    public void setDate(Object date) {
        mDate = date;
    }

    public Object getDateOfMarrige() {
        return mDateOfMarrige;
    }

    public void setDateOfMarrige(Object dateOfMarrige) {
        mDateOfMarrige = dateOfMarrige;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String dealerCode) {
        mDealerCode = dealerCode;
    }

    public Object getDealerMobile() {
        return mDealerMobile;
    }

    public void setDealerMobile(Object dealerMobile) {
        mDealerMobile = dealerMobile;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String dealerName) {
        mDealerName = dealerName;
    }

    public Double getDepositAmount() {
        return mDepositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        mDepositAmount = depositAmount;
    }

    public Double getDepositAmountPaid() {
        return mDepositAmountPaid;
    }

    public void setDepositAmountPaid(Double depositAmountPaid) {
        mDepositAmountPaid = depositAmountPaid;
    }

    public Double getDepositCost() {
        return mDepositCost;
    }

    public void setDepositCost(Double depositCost) {
        mDepositCost = depositCost;
    }

    public String getDepositPaid() {
        return mDepositPaid;
    }

    public void setDepositPaid(String depositPaid) {
        mDepositPaid = depositPaid;
    }

    public String getDepositPaidDate() {
        return mDepositPaidDate;
    }

    public void setDepositPaidDate(String depositPaidDate) {
        mDepositPaidDate = depositPaidDate;
    }

    public Object getDepositPayType() {
        return mDepositPayType;
    }

    public void setDepositPayType(Object depositPayType) {
        mDepositPayType = depositPayType;
    }

    public Object getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(Object doorNo) {
        mDoorNo = doorNo;
    }

    public Object getEmail() {
        return mEmail;
    }

    public void setEmail(Object email) {
        mEmail = email;
    }

    public Object getEmail2() {
        return mEmail2;
    }

    public void setEmail2(Object email2) {
        mEmail2 = email2;
    }

    public Object getFlag() {
        return mFlag;
    }

    public void setFlag(Object flag) {
        mFlag = flag;
    }

    public Object getFlatName() {
        return mFlatName;
    }

    public void setFlatName(Object flatName) {
        mFlatName = flatName;
    }

    public Object getFlatNo() {
        return mFlatNo;
    }

    public void setFlatNo(Object flatNo) {
        mFlatNo = flatNo;
    }

    public Object getFloor() {
        return mFloor;
    }

    public void setFloor(Object floor) {
        mFloor = floor;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInstallationBy() {
        return mInstallationBy;
    }

    public void setInstallationBy(String installationBy) {
        mInstallationBy = installationBy;
    }

    public String getInstalledBy() {
        return mInstalledBy;
    }

    public void setInstalledBy(String installedBy) {
        mInstalledBy = installedBy;
    }

    public String getInstalledDate() {
        return mInstalledDate;
    }

    public void setInstalledDate(String installedDate) {
        mInstalledDate = installedDate;
    }

    public String getLandmark() {
        return mLandmark;
    }

    public void setLandmark(String landmark) {
        mLandmark = landmark;
    }

    public Double getLangitude() {
        return mLangitude;
    }

    public void setLangitude(Double langitude) {
        mLangitude = langitude;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        mLatitude = latitude;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public Object getMobile2() {
        return mMobile2;
    }

    public void setMobile2(Object mobile2) {
        mMobile2 = mobile2;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPayType() {
        return mPayType;
    }

    public void setPayType(String payType) {
        mPayType = payType;
    }

    public Long getPid() {
        return mPid;
    }

    public void setPid(Long pid) {
        mPid = pid;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public Boolean getPump() {
        return mPump;
    }

    public void setPump(Boolean pump) {
        mPump = pump;
    }

    public Object getQRCode() {
        return mQRCode;
    }

    public void setQRCode(Object qRCode) {
        mQRCode = qRCode;
    }

    public String getReasonForInactive() {
        return mReasonForInactive;
    }

    public void setReasonForInactive(String reasonForInactive) {
        mReasonForInactive = reasonForInactive;
    }

    public String getReceiptNo() {
        return mReceiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        mReceiptNo = receiptNo;
    }

    public Long getReceiptNoFormat() {
        return mReceiptNoFormat;
    }

    public void setReceiptNoFormat(Long receiptNoFormat) {
        mReceiptNoFormat = receiptNoFormat;
    }

    public Object getReferEmail() {
        return mReferEmail;
    }

    public void setReferEmail(Object referEmail) {
        mReferEmail = referEmail;
    }

    public Object getReferMobile() {
        return mReferMobile;
    }

    public void setReferMobile(Object referMobile) {
        mReferMobile = referMobile;
    }

    public Object getReferName() {
        return mReferName;
    }

    public void setReferName(Object referName) {
        mReferName = referName;
    }

    public Object getReferUsertype() {
        return mReferUsertype;
    }

    public void setReferUsertype(Object referUsertype) {
        mReferUsertype = referUsertype;
    }

    public Long getRegId() {
        return mRegId;
    }

    public void setRegId(Long regId) {
        mRegId = regId;
    }

    public Object getSalesExecutive() {
        return mSalesExecutive;
    }

    public void setSalesExecutive(Object salesExecutive) {
        mSalesExecutive = salesExecutive;
    }

    public Object getSplRate() {
        return mSplRate;
    }

    public void setSplRate(Object splRate) {
        mSplRate = splRate;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public Object getStreet() {
        return mStreet;
    }

    public void setStreet(Object street) {
        mStreet = street;
    }

    public Object getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(Object updatedDate) {
        mUpdatedDate = updatedDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public Object getUserType() {
        return mUserType;
    }

    public void setUserType(Object userType) {
        mUserType = userType;
    }

    public String getWhatsAppNo() {
        return mWhatsAppNo;
    }

    public void setWhatsAppNo(String whatsAppNo) {
        mWhatsAppNo = whatsAppNo;
    }

}
