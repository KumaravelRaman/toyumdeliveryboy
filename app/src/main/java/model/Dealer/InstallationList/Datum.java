
package model.Dealer.InstallationList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Area")
    private String mArea;
    @SerializedName("CansLimit")
    private Long mCansLimit;
    @SerializedName("Center")
    private String mCenter;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Comments")
    private String mComments;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerStatus")
    private String mCustomerStatus;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("CustomerZone")
    private String mCustomerZone;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DepositAmount")
    private Long mDepositAmount;
    @SerializedName("DepositPaidDate")
    private String mDepositPaidDate;
    @SerializedName("DoorNo")
    private String mDoorNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("InstallationDate")
    private String mInstallationDate;
    @SerializedName("InstalledBy")
    private Object mInstalledBy;
    @SerializedName("InstalledDate")
    private String mInstalledDate;
    @SerializedName("Landmark")
    private String mLandmark;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("PayType")
    private String mPayType;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("ReasonForInactive")
    private Object mReasonForInactive;
    @SerializedName("SalesExecutive")
    private String mSalesExecutive;
    @SerializedName("SplRate")
    private String mSplRate;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("UpdatedDate")
    private Object mUpdatedDate;
    @SerializedName("UserName")
    private String mUserName;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String Area) {
        mArea = Area;
    }

    public Long getCansLimit() {
        return mCansLimit;
    }

    public void setCansLimit(Long CansLimit) {
        mCansLimit = CansLimit;
    }

    public String getCenter() {
        return mCenter;
    }

    public void setCenter(String Center) {
        mCenter = Center;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getComments() {
        return mComments;
    }

    public void setComments(String Comments) {
        mComments = Comments;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public String getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(String CustomerStatus) {
        mCustomerStatus = CustomerStatus;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String CustomerType) {
        mCustomerType = CustomerType;
    }

    public String getCustomerZone() {
        return mCustomerZone;
    }

    public void setCustomerZone(String CustomerZone) {
        mCustomerZone = CustomerZone;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String DealerCode) {
        mDealerCode = DealerCode;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String DealerName) {
        mDealerName = DealerName;
    }

    public Long getDepositAmount() {
        return mDepositAmount;
    }

    public void setDepositAmount(Long DepositAmount) {
        mDepositAmount = DepositAmount;
    }

    public String getDepositPaidDate() {
        return mDepositPaidDate;
    }

    public void setDepositPaidDate(String DepositPaidDate) {
        mDepositPaidDate = DepositPaidDate;
    }

    public String getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(String DoorNo) {
        mDoorNo = DoorNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public String getInstallationDate() {
        return mInstallationDate;
    }

    public void setInstallationDate(String InstallationDate) {
        mInstallationDate = InstallationDate;
    }

    public Object getInstalledBy() {
        return mInstalledBy;
    }

    public void setInstalledBy(Object InstalledBy) {
        mInstalledBy = InstalledBy;
    }

    public String getInstalledDate() {
        return mInstalledDate;
    }

    public void setInstalledDate(String InstalledDate) {
        mInstalledDate = InstalledDate;
    }

    public String getLandmark() {
        return mLandmark;
    }

    public void setLandmark(String Landmark) {
        mLandmark = Landmark;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String Password) {
        mPassword = Password;
    }

    public String getPayType() {
        return mPayType;
    }

    public void setPayType(String PayType) {
        mPayType = PayType;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String Pincode) {
        mPincode = Pincode;
    }

    public Object getReasonForInactive() {
        return mReasonForInactive;
    }

    public void setReasonForInactive(Object ReasonForInactive) {
        mReasonForInactive = ReasonForInactive;
    }

    public String getSalesExecutive() {
        return mSalesExecutive;
    }

    public void setSalesExecutive(String SalesExecutive) {
        mSalesExecutive = SalesExecutive;
    }

    public String getSplRate() {
        return mSplRate;
    }

    public void setSplRate(String SplRate) {
        mSplRate = SplRate;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String Street) {
        mStreet = Street;
    }

    public Object getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(Object UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String UserName) {
        mUserName = UserName;
    }

}
