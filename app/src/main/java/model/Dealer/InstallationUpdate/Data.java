
package model.Dealer.InstallationUpdate;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Amount")
    private String mAmount;
    @SerializedName("Cans")
    private String mCans;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("Id")
    private Long mId;

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String Amount) {
        mAmount = Amount;
    }

    public String getCans() {
        return mCans;
    }

    public void setCans(String Cans) {
        mCans = Cans;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String DealerCode) {
        mDealerCode = DealerCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

}
