
package model.Telecaller.GeneralCustomerList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("CallStatus")
    private Object mCallStatus;
    @SerializedName("CompanyName")
    private String mCompanyName;
    @SerializedName("createddate")
    private String mCreateddate;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("ExcelFile")
    private String mExcelFile;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Name")
    private Object mName;
    @SerializedName("Status")
    private Object mStatus;
    @SerializedName("updateddate")
    private Object mUpdateddate;

    public Object getCallStatus() {
        return mCallStatus;
    }

    public void setCallStatus(Object CallStatus) {
        mCallStatus = CallStatus;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String CompanyName) {
        mCompanyName = CompanyName;
    }

    public String getCreateddate() {
        return mCreateddate;
    }

    public void setCreateddate(String createddate) {
        mCreateddate = createddate;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getExcelFile() {
        return mExcelFile;
    }

    public void setExcelFile(String ExcelFile) {
        mExcelFile = ExcelFile;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public Object getName() {
        return mName;
    }

    public void setName(Object Name) {
        mName = Name;
    }

    public Object getStatus() {
        return mStatus;
    }

    public void setStatus(Object Status) {
        mStatus = Status;
    }

    public Object getUpdateddate() {
        return mUpdateddate;
    }

    public void setUpdateddate(Object updateddate) {
        mUpdateddate = updateddate;
    }

}
