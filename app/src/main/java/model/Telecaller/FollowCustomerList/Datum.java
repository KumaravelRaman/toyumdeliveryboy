
package model.Telecaller.FollowCustomerList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Appointment")
    private String mAppointment;
    @SerializedName("CallStatus")
    private String mCallStatus;
    @SerializedName("CompanyName")
    private String mCompanyName;
    @SerializedName("createddate")
    private String mCreateddate;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("ExcelFile")
    private String mExcelFile;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Remarks")
    private Object mRemarks;
    @SerializedName("Reason")
    private Object mReason;
    @SerializedName("TelecallerId")
    private String mTelecallerId;
    @SerializedName("updateddate")
    private String mUpdateddate;

    public String getAppointment() {
        return mAppointment;
    }

    public void setAppointment(String Appointment) {
        mAppointment = Appointment;
    }

    public String getCallStatus() {
        return mCallStatus;
    }

    public void setCallStatus(String CallStatus) {
        mCallStatus = CallStatus;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String CompanyName) {
        mCompanyName = CompanyName;
    }

    public String getCreateddate() {
        return mCreateddate;
    }

    public void setCreateddate(String createddate) {
        mCreateddate = createddate;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getExcelFile() {
        return mExcelFile;
    }

    public void setExcelFile(String ExcelFile) {
        mExcelFile = ExcelFile;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        mName = Name;
    }

    public Object getRemarks() {
        return mRemarks;
    }

    public void setRemarks(Object Remarks) {
        mRemarks = Remarks;
    }
    public Object getReason() {
        return mReason;
    }

    public void setReason(Object Reason) {
        mReason = Reason;
    }


    public String getTelecallerId() {
        return mTelecallerId;
    }

    public void setTelecallerId(String TelecallerId) {
        mTelecallerId = TelecallerId;
    }

    public String getUpdateddate() {
        return mUpdateddate;
    }

    public void setUpdateddate(String updateddate) {
        mUpdateddate = updateddate;
    }

}
