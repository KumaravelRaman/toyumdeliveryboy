
package model.Telecaller.UpdateTelecaller;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Appointment")
    private String mAppointment;
    @SerializedName("CallStatus")
    private String mCallStatus;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Remarks")
    private String mRemarks;

    public String getAppointment() {
        return mAppointment;
    }

    public void setAppointment(String Appointment) {
        mAppointment = Appointment;
    }

    public String getCallStatus() {
        return mCallStatus;
    }

    public void setCallStatus(String CallStatus) {
        mCallStatus = CallStatus;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        mName = Name;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String Remarks) {
        mRemarks = Remarks;
    }

}
