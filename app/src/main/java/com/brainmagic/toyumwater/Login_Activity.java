package com.brainmagic.toyumwater;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.ForgetPasswordAlert;
import dealer.Dealer_Installation_Update_Activity;
import model.Login.LoginResult;
import sharedpreference.Shared;
import retroclient.RetroClient;
import customer.Customer_Welcome_Activity;
import dealer.Dealer_Welcome_Activity;
import fcm.Config;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import telecaller.Telecaller_Followup_Activity;
import telecaller.Telecaller_Home_Activity;

public class Login_Activity extends AppCompatActivity {
    EditText Username;
    Button Login;
    ImageView Back, Home;
    String s_user, s_password;
    Alert box = new Alert(Login_Activity.this);
    NetworkConnection network = new NetworkConnection(Login_Activity.this);
    String CustomerName, CustomerCode, CustomerUserName, CustomerPassword;
    String DealerName, DealerCode, DealerUserName, DealerPassword;
    String S_Usertype = "", S_Name = "", S_Code = "", S_Username = "", S_Address = "", S_Canslimit = "";
    String door_no = "",street_name = "",area = "";
    private SharedPreferences myshare;
    private SharedPreferences myshare2;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Username = (EditText) findViewById(R.id.username);
//        Password = (EditText) findViewById(R.id.password);
        Login = (Button) findViewById(R.id.login);
        Back = (ImageView) findViewById(R.id.back);
        Home = (ImageView) findViewById(R.id.home);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        myshare2 = getSharedPreferences(Config.SHARED_PREF, 0);
        editor = myshare.edit();

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_user = Username.getText().toString();
//                s_password = Password.getText().toString();
                if (s_user.equals("")) {
                    Username.setFocusable(true);
                    Username.setError("Enter Username");
                }
//                else if (s_password.equals("")) {
////                    Password.setFocusable(true);
////                    Password.setError("Enter Password");
//                }
                else {
                    checkInternet();
                }
            }
        });
    }


    private void checkInternet() {
        if (network.CheckInternet()) {
            userSignIn();
        } else {
            box.showAlertbox(getString(R.string.no_network));
        }
    }

    private void userSignIn() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Logging...");
            progressDialog.show();

            APIService service = RetroClient.getApiService();

            // Calling JSON

//            Call<LoginResult> call = service.userLogin(s_user, s_password, (int) myshare.getLong(Shared.K_RegisterID,0));
            Call<LoginResult> call = service.userLogin(s_user, s_user, (int) myshare.getLong(Shared.K_RegisterID,0));

            call.enqueue(new Callback<LoginResult>() {
                @Override
                public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
                    progressDialog.dismiss();



                    switch (response.body().getResult()) {


                            case "Dealer":
                                finish();
                                S_Usertype = response.body().getData().getUserType();
                                S_Name = response.body().getData().getDealerName();
                                S_Code = response.body().getData().getDealerCode();
                                S_Username = response.body().getData().getUserName();
//                                S_Password = response.body().getData().getPassword();

//                                S_Address = response.body().getData().getAddress();
//                                door_no=response.body().getData().getDoorNo();
//                                street_name=response.body().getData().getStreet();
//                                area=response.body().getData().getArea();

                                editor.putBoolean(Shared.K_Login, true);
                                editor.putString(Shared.K_Usertype, S_Usertype);
                                editor.putString(Shared.K_Name, S_Name);
                                editor.putString(Shared.K_Code, S_Code);
                                editor.putString(Shared.K_Username, S_Username);
//                                editor.putString(Shared.K_Password, S_Password);

                                 editor.putString(Shared.K_doorno,door_no);
                               editor.putString(Shared.K_area,area);
                              editor.putString(Shared.K_streetname,street_name);

                                editor.putLong(Shared.K_Id, response.body().getData().getId());
                                editor.commit();
                                startActivity(new Intent(getApplicationContext(), Dealer_Welcome_Activity.class));
                                break;



                            case "Invalid User":
                                box.showAlertbox("Kindly check your Username or Password !");
                                break;

                            default:
//                                box.showAlertbox(getString(R.string.connection_slow));
                                break;

                        }

                }

                @Override
                public void onFailure(Call<LoginResult> call, Throwable t) {
                    progressDialog.dismiss();
                    Alert box = new Alert(Login_Activity.this);
           //         Log.v("onFailure",t.getMessage());
//                    box.showAlertbox(getString(R.string.connection_slow));
                }

            });
        } catch (Exception ex) {
            ex.getMessage();
            Log.v("Error", ex.getMessage());
        }
    }


    public void Forget_Password(View view) {
        ForgetPasswordAlert forget = new ForgetPasswordAlert(Login_Activity.this);
        forget.showLoginbox();
    }
    public void Popup_Menu(View view) {
        PopupMenu popupMenu = new PopupMenu(Login_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(getApplicationContext(), Aboutus_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent services = new Intent(getApplicationContext(), Product_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.packaging:
                        Intent products = new Intent(getApplicationContext(),Packaging_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.login:
                        Intent login = new Intent(getApplicationContext(), Login_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.quality:
                        Intent quality = new Intent(getApplicationContext(), QualityControl_Activity.class);
                        startActivity(quality);
                        return true;
                    case R.id.contact:
                        Intent contact = new Intent(getApplicationContext(), Contact_Activity.class);
                        startActivity(contact);
                        return true;
                    case R.id.help:
                        Intent help = new Intent(getApplicationContext(), Helpdesk_Activity.class);
                        startActivity(help);
                        return true;
                    case R.id.documents:
                        /*Intent documents = new Intent(getApplicationContext(), .class);
                        startActivity(documents);
                        return true;*/


                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu);
        popupMenu.getMenu().findItem(R.id.login).setVisible(false);
        popupMenu.show();
    }


}
