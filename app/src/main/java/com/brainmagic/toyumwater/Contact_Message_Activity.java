package com.brainmagic.toyumwater;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import logout.logout;
import model.Contact.SendMessage;
import retroclient.RetroClient;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Contact_Message_Activity extends AppCompatActivity {
    private MaterialSpinner OrdertypeSpinner;
    private ArrayList<String> OrdertypeList;
    Alert box = new Alert(Contact_Message_Activity.this);
    AlertDialogue alert = new AlertDialogue(Contact_Message_Activity.this);
    NetworkConnection network = new NetworkConnection(Contact_Message_Activity.this);
    Toasts Toaster = new Toasts(Contact_Message_Activity.this);

    String S_Name, S_Email, S_Subject, S_Order, S_Message;
    private EditText Name, Email, Subject, Message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_message);
        OrdertypeSpinner = (MaterialSpinner) findViewById(R.id.refill);

        Name = (EditText) findViewById(R.id.name);
        Email = (EditText) findViewById(R.id.email);
        Subject = (EditText) findViewById(R.id.subject);
        Message = (EditText) findViewById(R.id.message);

        OrdertypeList = new ArrayList<String>();
        OrdertypeList.add("Select Order type");
        OrdertypeList.add("Water Refill");
        OrdertypeList.add("Order Now");

        OrdertypeSpinner.setBackgroundResource(R.drawable.autotextback);
        OrdertypeSpinner.setItems(OrdertypeList);
        OrdertypeSpinner.setPadding(30, 0, 0, 0);
        OrdertypeSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                S_Order = item.toString();
                Log.v("Usertype", S_Order);
            }
        });
    }

    public void Send_Message(View view) {
        S_Name = Name.getText().toString();
        S_Email = Email.getText().toString();
        S_Subject = Subject.getText().toString();
        S_Order = OrdertypeSpinner.getText().toString();
        S_Message = Message.getText().toString();

        if (S_Name.isEmpty()) {
            Name.setFocusable(true);
            Name.setError("Please Enter Name");
        } else if (S_Email.isEmpty()) {
            Email.setFocusable(true);
            Email.setError("Please Enter Mobile number");
        } else if (S_Subject.isEmpty()) {
            Subject.setFocusable(true);
            Subject.setError("Please Enter Subject");
        } else if (S_Order.equals("Select Order type"))
            Toaster.ShowErrorToast("Please select Order type");
        else if (S_Message.equals("")) {
            Message.setFocusable(true);
            Message.setError("Please Enter Message");
        } else if (network.CheckInternet()) {
            //Toaster.ShowErrorToast("Success");
            Message_Sending();

        } else {
            box.showAlertbox(getString(R.string.no_network));
        }
    }
    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    private void Message_Sending() {


        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Sending Email...");
            progressDialog.show();

            APIService service = RetroClient.getApiService();

            // Calling JSON

            Call<SendMessage> call = service.SEND_MESSAGE_CALL(S_Name, S_Email, S_Subject, S_Order, S_Message);
            call.enqueue(new Callback<SendMessage>() {
                @Override
                public void onResponse(Call<SendMessage> call, Response<SendMessage> response) {
                    progressDialog.dismiss();

                    switch (response.body().getResult()) {
                        case "Success":
                            alert.showAlertbox("Email sent successfully! Thank you!");
                            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });

                            break;

                        case "Failure":
                            box.showAlertbox("Error in Sending Mail! Please try again!");
                            break;

                        default:
                            box.showAlertbox(getString(R.string.connection_slow));
                            break;

                    }

                }

                @Override
                public void onFailure(Call<SendMessage> call, Throwable t) {
                    progressDialog.dismiss();

                    box.showAlertbox(getString(R.string.connection_slow));
                }

            });
        } catch (Exception ex) {
            ex.getMessage();
            Log.v("Error", ex.getMessage());
        }
    }

    public void Popup_Menu(View view) {
    }
}
