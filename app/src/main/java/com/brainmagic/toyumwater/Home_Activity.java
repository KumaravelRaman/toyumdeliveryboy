package com.brainmagic.toyumwater;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import logout.logout;
import sharedpreference.Shared;
import customer.Customer_Welcome_Activity;
import dealer.Dealer_Welcome_Activity;
import telecaller.Telecaller_Followup_Activity;
import telecaller.Telecaller_Home_Activity;

public class Home_Activity extends AppCompatActivity {
    @BindView(R.id.header_layout) LinearLayout Headerlayout;
    //LinearLayout Headerlayout;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        ImageView Logout = Headerlayout.findViewById(R.id.logout_image);
        RelativeLayout back_layout =  Headerlayout.findViewById(R.id.back_layout);
        back_layout.setVisibility(View.GONE);
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();


        if (myshare.getBoolean(Shared.K_Login, false))
        {
            Logout.setVisibility(View.VISIBLE);
        }else
            Logout.setVisibility(View.INVISIBLE);

    }
    public void Popup_Menu(View view) {
        PopupMenu popupMenu = new PopupMenu(Home_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(getApplicationContext(), Aboutus_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent services = new Intent(getApplicationContext(), Product_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.packaging:
                        Intent products = new Intent(getApplicationContext(),Packaging_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.login:
                        if (myshare.getBoolean(Shared.K_Login, false)) {
                            switch (myshare.getString(Shared.K_Usertype, "")) {
                                case "Customer":
                                    startActivity(new Intent(getApplicationContext(), Customer_Welcome_Activity.class));
                                    break;
                                case "Dealer":
                                    startActivity(new Intent(getApplicationContext(), Dealer_Welcome_Activity.class));
                                    break;
                                case "Telecaller":
                                    startActivity(new Intent(getApplicationContext(), Telecaller_Home_Activity.class));
                                    break;
                            }
                        } else {
                            startActivity(new Intent(getApplicationContext(), Login_Activity.class));
                        }
                        return true;
                    case R.id.quality:
                         Intent quality = new Intent(getApplicationContext(), QualityControl_Activity.class);
                        startActivity(quality);
                        return true;
                    case R.id.contact:
                         Intent contact = new Intent(getApplicationContext(), Contact_Activity.class);
                        startActivity(contact);
                        return true;
                    case R.id.help:
                        Intent help = new Intent(getApplicationContext(), Helpdesk_Activity.class);
                        startActivity(help);
                        return true;
                    case R.id.documents:
                        /*Intent documents = new Intent(getApplicationContext(), .class);
                        startActivity(documents);
                        return true;*/


                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu);
        popupMenu.show();
    }

    public void Log_Out(View view) {

        new logout(this).log_out();
    }

    public void About_Us(View view) {
        startActivity(new Intent(Home_Activity.this,Aboutus_Activity.class));
    }

    public void Packaging(View view) {
        startActivity(new Intent(Home_Activity.this,Packaging_Activity.class));
    }

    public void Products(View view) {
        startActivity(new Intent(Home_Activity.this,Product_Activity.class));

    }

    public void Login(View view) {
        if (myshare.getBoolean(Shared.K_Login, false)) {
            switch (myshare.getString(Shared.K_Usertype, "")) {
                case "Customer":
                    startActivity(new Intent(getApplicationContext(), Customer_Welcome_Activity.class));
                    break;
                case "Dealer":
                    startActivity(new Intent(getApplicationContext(), Dealer_Welcome_Activity.class));
                    break;
                case "Telecaller":
                    startActivity(new Intent(getApplicationContext(), Telecaller_Home_Activity.class));
                    break;
            }
        } else {
            startActivity(new Intent(getApplicationContext(), Login_Activity.class));
        }
    }

    public void Documents(View view) {
    }

    public void Help_Desk(View view) {
        startActivity(new Intent(getApplicationContext(), Helpdesk_Activity.class));
    }

    public void Contact_Us(View view) {
        startActivity(new Intent(getApplicationContext(), Contact_Activity.class));

    }

    public void Quality_Control(View view) {
        startActivity(new Intent(Home_Activity.this,QualityControl_Activity.class));

    }

    public void Create_Customer(View view) {
        startActivity(new Intent(Home_Activity.this,Create_Customer_Activity.class));
    }

    public void Referral_Scheme(View view) {
        startActivity(new Intent(Home_Activity.this,Referall_Activity.class));

    }
}
