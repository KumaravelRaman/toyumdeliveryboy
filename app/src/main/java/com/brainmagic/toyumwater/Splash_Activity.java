package com.brainmagic.toyumwater;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import dealer.Dealer_Welcome_Activity;
import sharedpreference.Shared;

public class Splash_Activity extends AppCompatActivity {
    private static final long SPLASH_DISPLAY_LENGTH = 1500;
    Boolean isLogin;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        isLogin = myshare.getBoolean(Shared.K_Login, false);
        //editor.putInt("VERSION",15).commit();

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (isLogin) {
                    Intent mainIntent = new Intent(Splash_Activity.this, Dealer_Welcome_Activity.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent mainIntent2 = new Intent(Splash_Activity.this, Login_Activity.class);
                    mainIntent2.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(mainIntent2);
                    finish();
                }


                /* Create an Intent that will start the Menu-Activity. */

            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}