package com.brainmagic.toyumwater;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import fragments.Quality_Four;
import fragments.Quality_One;
import fragments.Quality_Three;
import fragments.Quality_Two;
import logout.logout;

public class QualityControl_Activity extends FragmentActivity {
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static final int NUM_PAGES = 4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quality);



        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new QualityControl_Activity.ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0)
                return new Quality_One();
            else if (position == 1)
                return new Quality_Two();
            else if (position == 2)
                return new Quality_Three();
            else
                return new Quality_Four();
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }



    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
        PopupMenu popupMenu = new PopupMenu(QualityControl_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(getApplicationContext(), Aboutus_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent services = new Intent(getApplicationContext(), Product_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.packaging:
                        Intent products = new Intent(getApplicationContext(),Packaging_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.login:
                        Intent login = new Intent(getApplicationContext(), Login_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.quality:
                        Intent quality = new Intent(getApplicationContext(), QualityControl_Activity.class);
                        startActivity(quality);
                        return true;
                    case R.id.contact:
                        Intent contact = new Intent(getApplicationContext(), Contact_Activity.class);
                        startActivity(contact);
                        return true;
                    case R.id.help:
                        Intent help = new Intent(getApplicationContext(), Helpdesk_Activity.class);
                        startActivity(help);
                        return true;
                    case R.id.documents:
                        /*Intent documents = new Intent(getApplicationContext(), .class);
                        startActivity(documents);
                        return true;*/


                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu);
        popupMenu.getMenu().findItem(R.id.quality).setVisible(false);
        popupMenu.show();
    }

}
