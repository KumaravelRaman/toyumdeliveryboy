package com.brainmagic.toyumwater;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import alertbox.Alert;
import dealer.Dealer_Refillpending_Activity;
import model.Dealer.PendingcanList.Datum;
import network.NetworkConnection;

public class Maps_Activity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    NetworkConnection network = new NetworkConnection(this);
    Alert alert = new Alert(this);
    private ProgressDialog progressbar;
    private ArrayList<Double> LatitudeList;
    private ArrayList<Double> LongtitudeList;
    private ArrayList<String> DescriptionList;
    //private ArrayList<String> AddressList;
    private ArrayList<LatLng> markerPoints;
    ArrayList<Datum> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        //   Bundle b = getIntent().getExtras();

        // if (null != b) {
        //ArrayList<Datum> arr = b.getParcelableArray("");
        dataList = new ArrayList<>();
        dataList = (ArrayList<Datum>) getIntent().getSerializableExtra("pendinglist");
            /*ArrayList<Datum> arr = new ArrayList<>();
            arr = getIntent().getParcelableArrayListExtra("pendinglist");*/
        //Log.i("List", "Passed Array List :: " + arr);
        //   }
        //AddressList = new ArrayList<String>();
        LatitudeList = new ArrayList<Double>();
        LongtitudeList = new ArrayList<Double>();
        DescriptionList = new ArrayList<String>();
        markerPoints = new ArrayList<LatLng>();


        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
      //new FindLatLongFromAddress().execute();
     /*   if (network.CheckInternet())
            new FindLatLongFromAddress().execute();
        else
            alert.showAlertbox(getString(R.string.no_network));*/
        //Load_coordinates();


        // Add a marker in Sydney and move the camera
        for (Datum data_item : dataList) {
            LatitudeList.add(data_item.getLatitude());
            LongtitudeList.add(data_item.getLongitude());
            markerPoints.add(new LatLng(data_item.getLatitude(), data_item.getLongitude()));
            DescriptionList.add(data_item.getCustomerName());
            //AddressList.add(rset.getString("cityname"));
        }

        LatLng sydney2 = new LatLng(LatitudeList.get(LatitudeList.size()-1), LongtitudeList.get(LatitudeList.size()-1));
        //LatLngBounds.Builder builder = LatLngBounds.builder();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int i = 0; i < LatitudeList.size(); i++) {
            LatLng sydney = new LatLng(LatitudeList.get(i), LongtitudeList.get(i));


            //LatLngBounds latlngbounds = new LatLngBounds(LatitudeList.get(i), LongtitudeList.get(i));
            mMap.addMarker(new MarkerOptions().position(sydney).title(DescriptionList.get(i)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))).showInfoWindow();
            builder.include(sydney);
        }

        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);
       /* LatLngBounds bounds = builder.build();

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.moveCamera(cu);*/
        //mMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        //mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
      //  mMap.animateCamera(cu);

   /*     LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
    }

    public class FindLatLongFromAddress extends AsyncTask<String, Void, String> {
        /*Geocoder coder = news Geocoder(MapsActivity.this);
        List<Address> address;
        Barcode.GeoPoint p1 = null;
        Address location;*/

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar = new ProgressDialog(Maps_Activity.this);
            progressbar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressbar.setMessage("Loading....");
            progressbar.setCancelable(false);
            progressbar.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
            //for(int i = 0; i  )
                for (Datum data_item : dataList) {
                    LatitudeList.add(data_item.getLatitude());
                    LongtitudeList.add(data_item.getLongitude());
                    markerPoints.add(new LatLng(data_item.getLatitude(), data_item.getLongitude()));
                    DescriptionList.add(data_item.getCustomerName());
                    //AddressList.add(rset.getString("cityname"));
                }

                if (DescriptionList.isEmpty()) {
                    return "nodata";
                }

                return "success";
            } catch (Exception e) {
                Log.v("back", e.getMessage());
                e.printStackTrace();
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.dismiss();

            //Map.addMarker(new MarkerOptions().position(sydney).title(DescriptionList.get(i)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            if (s.equals("success")) {

                try {
                    if (LatitudeList.size() > 0) {

                        /*String url = getUrl(markerPoints);
                        Log.d("onMapClick", url.toString());
                        FetchUrl FetchUrl = new FetchUrl();
                        // Start downloading json data from Google Directions API
                        FetchUrl.execute(url);*/
                        //move map camera
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(markerPoints.get(0)));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

                        for (int i = 0; i < LatitudeList.size(); i++) {
                            LatLng sydney = new LatLng(LatitudeList.get(i), LongtitudeList.get(i));
                           /* if (i <= 0) {
                                mMap.addMarker(new MarkerOptions().position(sydney).title(DescriptionList.get(i)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))).showInfoWindow();
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
                                mMap.animateCamera(CameraUpdateFactory.zoomTo(12));

                            } else if (i >= LatitudeList.size() - 1) {*/


                           // LatLng sydney = new LatLng(-34, 151);

//                            mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                                mMap.addMarker(new MarkerOptions().position(sydney).title(DescriptionList.get(i)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                //Map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
                          /*  } else {
                                mMap.addMarker(new MarkerOptions().position(sydney).title(DescriptionList.get(i)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                                //Map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
                            }*/
                        }
                       /* if (ActivityCompat.checkSelfPermission(Maps_Activity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }*/
                      //  mMap.setMyLocationEnabled(true);

                    }
                    else{
                        alert.showAlertbox("location not found !");
                    }
                } catch (Exception e) {
                    alert.showAlertbox("location not found !");
                }

            } else if (s.equals("nodata")) {
                alert.showAlertbox("location not found !");
            } else {
                alert.showAlertbox("Please check your network connection and try again.");
            }
        }
    }
}
