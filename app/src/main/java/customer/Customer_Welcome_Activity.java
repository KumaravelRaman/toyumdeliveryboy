package customer;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import apiservice.APIService;
import alertbox.Alert;
import logout.logout;
import model.Customer.CutomerRefillRequest.CustomerRefill;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Customer_Welcome_Activity extends AppCompatActivity {

    EditText Refillcans,Remarks;
    TextView Customer_Name,Customer_Code,Customer_Address,CansLimit;

    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);

    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private static final int PERMISSION_REQUEST = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_welcome);

        Customer_Name = (TextView)findViewById(R.id.name);
        Customer_Code = (TextView)findViewById(R.id.code);
        Customer_Address = (TextView)findViewById(R.id.address);
        CansLimit = (TextView)findViewById(R.id.canslimit);

        Refillcans = (EditText) findViewById(R.id.refillcans);
        Remarks = (EditText) findViewById(R.id.remarks);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        Customer_Name.setText(myshare.getString(Shared.K_Name,""));
        Customer_Code.setText(myshare.getString(Shared.K_Code,""));
        Customer_Address.setText(myshare.getString(Shared.K_Address,""));
        CansLimit.setText(myshare.getString(Shared.K_Canslimit,""));



    }
    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }


    public void history(View view) {
        startActivity(new Intent(this,Customer_Order_History_Activity.class));
    }

    /*public void delivery(View view) {
        startActivity(new Intent(this,Customer_Delivery.class));
    }*/

    public void complaint(View view) {
        startActivity(new Intent(this,Customer_Complaints_Activity.class));
    }

    public void Order(View view) {
        if (!Refillcans.getText().toString().equals("")) {

            if (network.CheckInternet()) {
                Refill_Order();
            } else {
                alert.showAlertbox(getString(R.string.no_network));
            }
        } else {
            Refillcans.setError("Kindly fill no. of cans");
            Refillcans.setFocusable(true);

        }
    }

    public void Change_Password(View view) {
        startActivity(new Intent(this,Customer_Changepassword_Activity.class));
    }





    private void Refill_Order() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        APIService service = RetroClient.getApiService();
        Call<CustomerRefill> call = service.Customer_Refill(myshare.getString(Shared.K_Code, ""), Refillcans.getText().toString(), Remarks.getText().toString());

        call.enqueue(new Callback<CustomerRefill>() {
            @Override
            public void onResponse(Call<CustomerRefill> call, Response<CustomerRefill> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                        alert.showAlertbox("Dear customer, your order placed successfully\n Your order No. is "+response.body().getData());
                        Refillcans.setText("");
                        Remarks.setText("");
                        Remarks.clearFocus();
                        break;
                    case "LimitExceded":
                        alert.showAlertbox("Sorry ! Your Order limit is "+ response.body().getData() + " cans");
                        break;
                        //PendingOrderExceed
                    case "PendingOrder":
                        alert.showAlertbox("Sorry ! You have already ordered " + response.body().getData()+ " cans");
                        break;
                    case "PendingOrderExceed":
                        alert.showAlertbox(response.body().getData());
                        break;

                    case "NotActive":
                        alert.showAlertbox("Your are not a active customer! Kindly contact Toyum for further information !");
                        break;
                    default :
                        alert.showAlertbox(getString(R.string.connection_slow));
                        break;
                }
            }

            @Override
            public void onFailure(Call<CustomerRefill> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
            }
        });


    }


    public void Log_Out(View view) {
        new logout(this).log_out();
    }


    public void View_Profile(View view) {


        if(network.CheckInternet())
        {
            selectoptions();//GetPermission();
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }
        //startActivity(new Intent(this,Customer_Profile_Activity.class));



    }
        /* if(myshare.getBoolean(Shared.K_Login,false))
    {
        editor.putBoolean(Shared.K_Login,false);
        editor.apply();
        startActivity(new Intent(getApplicationContext(),Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }*/
        void selectoptions(){
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.select_profile_options, null);
            alertDialog.setView(dialogView);


            Button Cancel = (Button) dialogView.findViewById(R.id.cancel);
            Button ViewProfile = (Button) dialogView.findViewById(R.id.viewprofile);
            Button Location_Update = (Button) dialogView.findViewById(R.id.location);
            Button ChangePassword = (Button) dialogView.findViewById(R.id.password);


            Cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }
            });
            Location_Update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    startActivity(new Intent(getApplicationContext(), Customer_Location_Update_Activity.class));

                }
            });
            ViewProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    startActivity(new Intent(getApplicationContext(), Customer_Profile_Activity.class));

                }

            });
            ChangePassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    startActivity(new Intent(getApplicationContext(), Customer_Changepassword_Activity.class));

                }

            });

            alertDialog.show();
        }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Snackbar.make(findViewById(R.id.customer_home), "You need give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            //your action here
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST);
                        }
                    }).show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST);
                }
            } else {
                selectoptions();
            }
        } else

        {
            selectoptions();
        }

    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Snackbar.make(findViewById(R.id.customer_home), "Permission Granted",
                    Snackbar.LENGTH_LONG).show();
            selectoptions();

        } else {

            Snackbar.make(findViewById(R.id.customer_home), "Permission denied",
                    Snackbar.LENGTH_LONG).show();

        }
    }

    public void Popup_Menu(View view) {
        PopupMenu popupMenu = new PopupMenu(Customer_Welcome_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.c_history:
                        Intent about = new Intent(getApplicationContext(), Customer_Order_History_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.c_customer:
                        Intent services = new Intent(getApplicationContext(), Customer_Welcome_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.c_profile:
                        Intent products = new Intent(getApplicationContext(),Customer_Profile_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.c_password:
                        Intent login = new Intent(getApplicationContext(), Customer_Changepassword_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.c_suggestions:
                        Intent suggestion = new Intent(getApplicationContext(), Customer_Complaints_Activity.class);
                        startActivity(suggestion);
                        return true;

                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu_customer);
        popupMenu.getMenu().findItem(R.id.c_customer).setVisible(false);
        popupMenu.show();
    }
}
