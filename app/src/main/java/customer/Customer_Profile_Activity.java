package customer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;
import com.brainmagic.toyumwater.WebView_Activity;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import logout.logout;
import model.Customer.CustomerInfo.Customerinfo;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Customer_Profile_Activity extends AppCompatActivity {

    private TextView CustomerName,CustomerCode,CustomerMobile,CustomerEmail,CansLimit,DepositAmount,DepositAmountPaid,DealerName,DealerMobile,DoorNo,Street,Address,Area,City,Landmark;
    private Alert alert = new Alert(this);
    private NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private Customerinfo info ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);
        CustomerName = (TextView) findViewById(R.id.name);
        CustomerCode = (TextView) findViewById(R.id.code);
        CustomerMobile = (TextView) findViewById(R.id.mobile);
        CustomerEmail = (TextView) findViewById(R.id.email);
        CansLimit = (TextView) findViewById(R.id.canslimit);
        DepositAmount = (TextView) findViewById(R.id.deposit);
        DepositAmountPaid = (TextView) findViewById(R.id.depositpaid);
        DealerName = (TextView) findViewById(R.id.dealername);
        DealerMobile = (TextView) findViewById(R.id.dealermobile);
        DoorNo = (TextView) findViewById(R.id.door);
        Street = (TextView) findViewById(R.id.street);
        Address = (TextView) findViewById(R.id.address);
        Area = (TextView) findViewById(R.id.area);
        City = (TextView) findViewById(R.id.city);
        Landmark = (TextView) findViewById(R.id.landmark);
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);


        if(network.CheckInternet())
        {
            Get_CustomerInfo();
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }

    }

    @Override
    protected void onRestart() {
        recreate();
        super.onRestart();
    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }



    public void Edit_Profile(View view) {
       // EditProfile Edit = new EditProfile(this,info);
       // Edit.show_edit_profile();
        startActivity( new Intent(Customer_Profile_Activity.this,Customer_Editprofile_Activity.class));
    }


    private void Get_CustomerInfo() {

        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<Customerinfo> call = service.CUSTOMERINFO_CALL(myshare.getString(Shared.K_Code,""));
            call.enqueue(new Callback<Customerinfo>() {
                @Override
                public void onResponse(Call<Customerinfo> call, Response<Customerinfo> response) {
                    progressDialog.dismiss();
                    switch (response.body().getResult()) {
                        case "Success":
                        {
                            info = response.body();
                        CustomerName.setText(String.format("%s", response.body().getData().getCustomerName()));
                        CustomerCode.setText(String.format("%s", response.body().getData().getCustomerCode()));
                        CustomerMobile.setText(String.format("%s", response.body().getData().getMobile()));
                        CustomerEmail.setText(String.format("%s", response.body().getData().getEmail()));
                        CansLimit.setText(String.format("%s", response.body().getData().getCansLimit()));
                        DepositAmount.setText(String.format("%s", getString(R.string.RS)+" "+response.body().getData().getDepositAmount()+".00"));
                        DepositAmountPaid.setText(String.format("%s",  getString(R.string.RS)+" "+response.body().getData().getDepositAmountPaid()+".00"));
                        DealerName.setText(String.format("%s", response.body().getData().getDealerName()));
                        DealerMobile.setText(String.format("%s", response.body().getData().getDealerMobile()));
                        DoorNo.setText(String.format("%s", response.body().getData().getDoorNo()));
                        Street.setText(String.format("%s", response.body().getData().getStreet()));
                        Address.setText(String.format("%s", response.body().getData().getAddress()));
                        Area.setText(String.format("%s", response.body().getData().getArea()));
                        City.setText(String.format("%s", response.body().getData().getCity()));
                        Landmark.setText(String.format("%s", response.body().getData().getLandmark()));

                            break;
                        }
                        case "No data":
                        {
                            AlertDialogue alertnew = new AlertDialogue(Customer_Profile_Activity.this);
                            alertnew.showAlertbox("No details found");
                            alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        default: {
                            alert.showAlertbox(getString(R.string.connection_slow));
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<Customerinfo> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void Change_password(View view) {
        Intent Profileindent = new Intent(Customer_Profile_Activity.this,Customer_Changepassword_Activity.class);
        startActivity(Profileindent);
    }


    public void View_Reciept(View view) {
        startActivity( new Intent(Customer_Profile_Activity.this,WebView_Activity.class).putExtra("pdfurl",myshare.getString(Shared.K_Code,"")));

    }
    public void Popup_Menu(View view) {
        PopupMenu popupMenu = new PopupMenu(Customer_Profile_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.c_history:
                        Intent about = new Intent(getApplicationContext(), Customer_Order_History_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.c_customer:
                        Intent services = new Intent(getApplicationContext(), Customer_Welcome_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.c_profile:
                        Intent products = new Intent(getApplicationContext(),Customer_Profile_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.c_password:
                        Intent login = new Intent(getApplicationContext(), Customer_Changepassword_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.c_suggestions:
                        Intent suggestion = new Intent(getApplicationContext(), Customer_Complaints_Activity.class);
                        startActivity(suggestion);
                        return true;

                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu_customer);
        popupMenu.getMenu().findItem(R.id.c_profile).setVisible(false);
        popupMenu.show();
    }
}
