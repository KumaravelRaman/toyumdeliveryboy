package customer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import logout.logout;
import model.Customer.CustomerInfo.Customerinfo;
import model.Customer.EditCustomerInfo.EditCustomer;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Customer_Editprofile_Activity extends AppCompatActivity {
    private EditText CustomerName, CustomerMobile,CustomerEmail,Landmark,DoorNo,Sreet,Address,CustomerMobile2,CustomerEmail2;
    private Button Update;
    private Alert alert;
    private NetworkConnection network;
    private Toasts toaster;
    private SharedPreferences myshare;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_editprofile);

        CustomerName = (EditText) findViewById(R.id.customername);
        CustomerMobile = (EditText) findViewById(R.id.customermobile);
        CustomerEmail = (EditText) findViewById(R.id.customeremail);
        CustomerMobile2 = (EditText) findViewById(R.id.customermobile2);
        CustomerEmail2 = (EditText) findViewById(R.id.customeremail2);
        DoorNo = (EditText) findViewById(R.id.door);
        Sreet = (EditText) findViewById(R.id.street);
        Address = (EditText) findViewById(R.id.address);
        CustomerMobile = (EditText) findViewById(R.id.customermobile);
        Landmark = (EditText) findViewById(R.id.landmark);

        Update = (Button) findViewById(R.id.update);


        alert = new Alert(Customer_Editprofile_Activity.this);
        network = new NetworkConnection(Customer_Editprofile_Activity.this);
        toaster = new Toasts(Customer_Editprofile_Activity.this);
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (network.CheckInternet()) {
                    UpdateProfile();
                } else {
                    alert.showAlertbox(getString(R.string.no_network));
                }


            }
        });


        // Get

        if(network.CheckInternet())
        {
            Get_CustomerInfo();
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }



    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {

    }

    public void UpdateProfile() {
        final ProgressDialog progressDialog = new ProgressDialog(Customer_Editprofile_Activity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        APIService service = RetroClient.getApiService();

        Call<EditCustomer> call = service.EDIT_CUSTOMER_CALL(myshare.getString(Shared.K_Code, ""),

                CustomerMobile.getText().toString(),
                CustomerMobile2.getText().toString(),
                CustomerEmail.getText().toString(),
                CustomerEmail2.getText().toString()
        );
        call.enqueue(new Callback<EditCustomer>() {
            @Override
            public void onResponse(Call<EditCustomer> call, Response<EditCustomer> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success": {

                        AlertDialogue alertnew = new AlertDialogue(Customer_Editprofile_Activity.this);
                        alertnew.showAlertbox("Profile updated successfully");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });

                        break;
                    }
                    case "Failure": {
                        alert.showAlertbox("Error in updating !, Please try again !");
                        break;
                    }
                    default: {

                        alert.showAlertbox(getString(R.string.connection_slow));

                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<EditCustomer> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));

            }
        });
    }

    private void Get_CustomerInfo() {

        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<Customerinfo> call = service.CUSTOMERINFO_CALL(myshare.getString(Shared.K_Code,""));
            call.enqueue(new Callback<Customerinfo>() {
                @Override
                public void onResponse(Call<Customerinfo> call, Response<Customerinfo> response) {
                    progressDialog.dismiss();

                    switch (response.body().getResult()) {

                        case "Success":

                        {
                            CustomerName.setText(response.body().getData().getCustomerName());
                            CustomerMobile.setText(response.body().getData().getMobile());
                            CustomerEmail.setText(response.body().getData().getEmail());
                            CustomerMobile2.setText(String.format("%s",response.body().getData().getMobile2()));
                            CustomerEmail2.setText(String.format("%s",response.body().getData().getEmail2()));
                            DoorNo.setText(response.body().getData().getDoorNo());
                            Sreet.setText(response.body().getData().getStreet());
                            Address.setText(response.body().getData().getAddress());
                            Landmark.setText(response.body().getData().getLandmark());

                            break;
                        }
                        case "No data":
                        {
                            AlertDialogue alertnew = new AlertDialogue(Customer_Editprofile_Activity.this);
                            alertnew.showAlertbox("No details found");
                            alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;
                        }
                        default: {
                            alert.showAlertbox(getString(R.string.connection_slow));
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<Customerinfo> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
