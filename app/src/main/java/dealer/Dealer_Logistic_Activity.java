package dealer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import logout.logout;

public class Dealer_Logistic_Activity extends AppCompatActivity {
    RelativeLayout Filed_Cans_Recieve,Inventory_History,Cans_Delivery_Update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_logistic);

        Filed_Cans_Recieve = (RelativeLayout)findViewById(R.id.filledcans);
        Cans_Delivery_Update = (RelativeLayout)findViewById(R.id.cansdelivery);
        Inventory_History = (RelativeLayout)findViewById(R.id.inventoryhistory);


    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }





    public void Filed_Cans_Recieve(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Logistic_Filled_Cans_Activity.class));
    }

    public void Cans_Delivery_Update(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Logistic_Cans_Delivery_Activity.class));
    }

    public void Inventory_History(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Logistic_History_Activity.class));
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

}
