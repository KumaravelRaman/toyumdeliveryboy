package dealer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import logout.logout;

public class Dealer_Logistic_Cans_Delivery_Activity extends AppCompatActivity
{
    TextView DealerName,Source,Filledcans,InstalledCans,Date_return;
    EditText EmptyCansReturn,LeakageCansReturn,FilledCansReturn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_logistic_cans_delivery);

        DealerName=(TextView)findViewById(R.id.name);
        Source=(TextView)findViewById(R.id.source);
        Filledcans=(TextView)findViewById(R.id.filledcans);
        Date_return =(TextView)findViewById(R.id.date);
        InstalledCans=(TextView)findViewById(R.id.installedcans);
        EmptyCansReturn = (EditText)findViewById(R.id.empty);
        LeakageCansReturn = (EditText)findViewById(R.id.leakage);
        FilledCansReturn = (EditText)findViewById(R.id.filled);

    }


    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Submit_Cans_Delivery(View view) {
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }
}
