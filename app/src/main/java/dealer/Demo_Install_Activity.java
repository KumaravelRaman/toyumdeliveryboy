package dealer;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import adapter.demo_Installationadapter;
import alertbox.Alert;
import alertbox.AlertDialogue;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;
import customedittext.CustomEditText;
import customedittext.DrawableClickListener;
import logout.logout;
import model.Dealer.demopendinginstalls.Datum;
import model.Dealer.demopendinginstalls.DemoPendings;
import network.NetworkConnection;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedpreference.Shared;
import toaster.Toasts;

public class Demo_Install_Activity extends AppCompatActivity {

    @BindView(R.id.fromdate)
    CustomEditText Fromdate;
    @BindView(R.id.todate)
    CustomEditText Todate;
    TextView Name;
    private String S_FromDate,S_ToDate;
    private Date Datefrom,Dateto;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    Toasts toasts = new Toasts(this);
    private RecyclerView recyclerView;
    private List<Datum> DemoPendingslist;
    private HorizontalScrollView Horizontalscrollview;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_install);
        ButterKnife.bind(this);
        Name = (TextView) findViewById(R.id.name);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        Horizontalscrollview = (HorizontalScrollView) findViewById(R.id.HorizontalScrollView);
        DemoPendingslist = new ArrayList<>();

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        Name.setText(myshare.getString(Shared.K_Name,""));


        Fromdate.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable text) {
                // TODO Auto-generated method stub
                if (text.length() > 0) {
                    try {
                        Fromdate.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_action_cancel,0);
                    } catch (Exception ex) {
                        Log.v("afterTextChanged", ex.getMessage());
                    }
                }
            }
        });
        Todate.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable text) {
                // TODO Auto-generated method stub
                if (text.length() > 0) {
                    try {
                        Todate.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_action_cancel,0);
                    } catch (Exception ex) {
                        Log.v("afterTextChanged", ex.getMessage());
                    }
                }
            }
        });
        Fromdate.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case LEFT:
                        //Do something here
                        break;

                    case RIGHT:
                        Fromdate.getText().clear();
                        Fromdate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                        break;
                    default:
                        break;
                }
            }

        });


        Todate.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case LEFT:
                        //Do something here
                        break;

                    case RIGHT:
                        Todate.getText().clear();
                        Todate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                        break;
                    default:
                        break;
                }
            }

        });
        Fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Calendar now = Calendar.getInstance();
                final Calendar c = Calendar.getInstance();

                DatePickerDialog dpd = new DatePickerDialog(Demo_Install_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            DecimalFormat mFormat= new DecimalFormat("00");
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                Datefrom = c.getTime();

                                String myFormat = "yyyy-MM-dd"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                S_FromDate = sdf.format(c.getTime());
                                Log.v("S_ChequeDate", S_FromDate);
//yyyy-MM-dd
                                String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                                SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
                                Fromdate.setText(sdf2.format(c.getTime()));
                                Log.v("ChequeDate", Fromdate.getText().toString());

                        /*    PurchaseDate.setText(mFormat.format(dayOfMonth) + "/" + mFormat.format(monthOfYear + 1) + "/" + year);
                            S_Date  = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
*/


                            }
                        }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                dpd.show();
            }
        });
        Todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Calendar now = Calendar.getInstance();
                if(Datefrom != null){
                    final Calendar c = Calendar.getInstance();

                    final DatePickerDialog dpd = new DatePickerDialog(Demo_Install_Activity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                /*    DecimalFormat mFormat= new DecimalFormat("00");*/
                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    c.set(year, monthOfYear, dayOfMonth);
                                    Dateto = c.getTime();

                                    String myFormat = "yyyy-MM-dd"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    S_ToDate = sdf.format(c.getTime());

                                    Log.v("S_ChequeDate", S_ToDate);
//yyyy-MM-dd
                                    String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                                    SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);

                                    Todate.setText(sdf2.format(c.getTime()));
                                    Log.v("ChequeDate", Todate.getText().toString());

                            /*PurchaseDate.setText(mFormat.format(dayOfMonth) + "/" + mFormat.format(monthOfYear + 1) + "/" + year);
                            S_Date  = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;*/



                                }
                            }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                    dpd.getDatePicker().setMinDate(Datefrom.getTime());
                    dpd.show();
                }else{
                    toasts.ShowErrorToast("Select From date");
                }
            }
        });

        if(network.CheckInternet())
        {
            Get_InstallationList("","");
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }
    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    private void Get_InstallationList(String From,String To){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        APIService service = RetroClient.getApiService();

        Call<DemoPendings> call = service.DEMO_PENDINGS_CALL(myshare.getString(Shared.K_Code,""),From,To);
        call.enqueue(new Callback<DemoPendings>() {
            @Override
            public void onResponse(Call<DemoPendings> call, Response<DemoPendings> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                    {
                        DemoPendingslist = response.body().getData();
                        demo_Installationadapter  mAdapter = new demo_Installationadapter(Demo_Install_Activity.this, DemoPendingslist);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        break;
                    }
                    case "No data":
                    {
                        //alert.showAlertbox("No Installations found");
                        AlertDialogue alertnew = new AlertDialogue(Demo_Install_Activity.this);
                        Horizontalscrollview.setVisibility(View.GONE);
                        alertnew.showAlertbox("No Installations found for this Dealer");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });
                        //alert.
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                    default: {

                        alert.showAlertbox(getString(R.string.connection_slow));
                        Horizontalscrollview.setVisibility(View.GONE);
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<DemoPendings> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
                Horizontalscrollview.setVisibility(View.GONE);

            }
        });

    }


    public void Popup_Menu(View view) {
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    @Override
    protected void onResume() {
        //0 recreate();
        super.onResume();
    }

    @Override
    protected void onRestart() {
        recreate();
        super.onRestart();
    }

    public void Search(View view) {
        if(Fromdate.getText().toString().equals(""))
            toasts.ShowErrorToast("Select From date");
        else if(Todate.getText().toString().equals(""))
            toasts.ShowErrorToast("Select To date");
        else if(network.CheckInternet())
            Get_InstallationList(S_FromDate,S_ToDate);
        else
            alert.showAlertbox(getString(R.string.no_network));

    }
}
