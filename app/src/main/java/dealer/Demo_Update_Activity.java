package dealer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.text.DateFormat;
import java.util.Date;

import alertbox.Alert;
import alertbox.AlertDialogue;
import apiservice.APIService;
import hidekeyboard.HideSoftKeyboard;
import logout.logout;
import model.Dealer.DeliveryUpdate.DeliveryConfimation;
import model.Dealer.demoupdate.DemoInstallUpdate;
import network.NetworkConnection;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedpreference.Shared;

public class Demo_Update_Activity extends AppCompatActivity {


    TextView CustomerName, CustomerCode, Refillcans, Current_Date;
    EditText DeliveredCans;
    long Id;
    String Name, Code, Refill ;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_update);
        new HideSoftKeyboard().setupUI(findViewById(R.id.dealer_delivery_confirmation), this);
        CustomerName = (TextView) findViewById(R.id.name);
        CustomerCode = (TextView) findViewById(R.id.code);
        Refillcans = (TextView) findViewById(R.id.filledcans);

        Current_Date = (TextView) findViewById(R.id.date);

        DeliveredCans = (EditText) findViewById(R.id.deliveredcans);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        //DealerName.setText(String.format("Dealer : %s", myshare.getString(Shared.K_Name, "")));


        Id = getIntent().getLongExtra("id", 0);

        Log.v("id", String.format("%s", Id));
        Name = getIntent().getStringExtra("name");
        Code = getIntent().getStringExtra("code");

        Refill = String.format("%s", getIntent().getLongExtra("cans", 0));
        CustomerName.setText(Name);
        CustomerCode.setText(Code);
        Refillcans.setText(Refill);


        String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
        Current_Date.setText(currentDateTimeString);

    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }


    public void Log_Out(View view) {
        new logout(this).log_out();
    }


    public void Confirm(View view) {

        if (DeliveredCans.getText().toString().equals("")) {
            DeliveredCans.setError("Enter empty cans");
            DeliveredCans.setFocusable(true);
        }
        else if (DeliveredCans.getText().toString().equals("0")) {
            DeliveredCans.setError("Cans amount cannot be zero");
            DeliveredCans.setFocusable(true);
        } else if (network.CheckInternet()) {
            Confirm_delivery();
        } else {
            alert.showAlertbox(getString(R.string.no_network));
        }
    }

    public void Popup_Menu(View view) {
    }

    public void Confirm_delivery() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            APIService service = RetroClient.getApiService();

            Call<DemoInstallUpdate> call = service.DEMO_INSTALL_UPDATE_CALL((int) Id, Integer.parseInt(DeliveredCans.getText().toString()));
            call.enqueue(new Callback<DemoInstallUpdate>() {
                @Override
                public void onResponse(Call<DemoInstallUpdate> call, Response<DemoInstallUpdate> response) {
                    progressDialog.dismiss();
                    switch (response.body().getResult()) {
                        case "Success": {

                            AlertDialogue alertnew = new AlertDialogue(Demo_Update_Activity.this);
                            alertnew.showAlertbox("Confirmation updated Successfully");
                            alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            //finish();
                            break;
                        }
                        case "Already Updated": {
                            alert.showAlertbox("Confirmation already updated! Please check");

                            break;
                        }
                        default: {
                            alert.showAlertbox(getString(R.string.connection_slow));
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<DemoInstallUpdate> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox(getString(R.string.connection_slow));
                    //Horizontalscrollview.setVisibility(View.GONE);

                }
            });
        } catch (NumberFormatException e) {
            e.printStackTrace();
            alert.showAlertbox("Server error! Please try again!");
        }
    }


}
