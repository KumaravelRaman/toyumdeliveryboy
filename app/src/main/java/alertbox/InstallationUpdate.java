package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import com.brainmagic.toyumwater.R;
import com.jaredrummler.materialspinner.MaterialSpinner;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


import model.Dealer.InstallationList.Datum;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;

import static android.content.Context.MODE_PRIVATE;


public class InstallationUpdate {
	private Context context;
	private List<Datum> InstallationList;
	private MaterialSpinner spinner,Paymentspinner,PaymentTypespinner,Bankspinner;
	private TextView CustomerName, CustomerCode, DepositAmount,FilledcansRequested;
	private TableRow Row_PaymentType,Row_Bank,Row_ChequeNumber,Row_ChequeDate,Row_Amount;

	private EditText DepositAmountPaid, Filledcans,ChequeNumber, ChequeDate;
	private Button Update,Cancel;
	int Position;


	long Id, Deposit, cans;
	String Name, Code, S_QRCode,S_Payment,S_Paymenttype,S_Bank,S_ChequeNumber,S_ChequeDate,S_Amount;
	Alert alert;
	NetworkConnection network;
	Toasts toaster;
	private SharedPreferences myshare;
	private SharedPreferences.Editor editor;
	private AlertDialog alertDialog;
	Calendar myCalendar = Calendar.getInstance();


	public InstallationUpdate(Context con,List<Datum> data,int listPosition) {
		// TODO Auto-generated constructor stub
		this.context = con;
		this.alert = new Alert(context);
		this.network = new NetworkConnection(context);
		this.toaster = new Toasts(context);
		this.InstallationList = data;
		this.Position = listPosition;
			}

 	//editor = myshare.edit();
	public void showupdatebox()
	{
		myshare = context.getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
		alertDialog = new Builder(context).create();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.installation_update, null);
		alertDialog.setView(dialogView);

		spinner = (MaterialSpinner) dialogView.findViewById(R.id.qrcodespinner);
		Paymentspinner = (MaterialSpinner) dialogView.findViewById(R.id.payment);
		PaymentTypespinner = (MaterialSpinner) dialogView.findViewById(R.id.paymenttype);
		Bankspinner = (MaterialSpinner) dialogView.findViewById(R.id.bankspinner);
		spinner.setItems("Select", "Yes", "No");
		Paymentspinner.setItems("Select", "Yes", "No");
		Bankspinner.setItems("Select","Allahabad Bank","Andhra Bank","Au Small Finance Bank","Axis Bank","Bandhan Bank","Bank of Baroda - Corporate Banking","Bank of Baroda - Retail Banking","Bank of India","Bank of Maharashtra","Citibank","Canara Bank","Catholic Syrian Bank","Central Bank of India","City Union Bank","Corporation Bank","DCB Bank","Dena Bank","Dhanlaxmi Bank","Equitas small Finance Bank","Federal Bank","HDFC Bank","ICICI Bank","IDBI Bank","IDFC Bank","Indian Bank","Indian Overseas Bank","IndusInd Bank","Jammu and Kashmir Bank","Karnataka Bank","Karur Vysya Bank","Kotak Mahindra Bank","Lakshmi Vilas Bank","Nainital Bank","Oriental Bank of Commerce","Punjab & Sindh Bank","Punjab National Bank","RBL Bank","South Indian Bank","State Bank of Bikaner and Jaipur","State Bank of Hyderabad","State Bank of India","State Bank of Mysore","State Bank of Patiala","State Bank of Travancore","Syndicate Bank","Tamilnad Mercantile Bank Limited","U.P Agro Corporation Bank","UCO Bank","Union Bank of India","United Bank of India","Vijaya Bank","Yes Bank");
		PaymentTypespinner.setItems("Select", "Cash", "Cheque");


		Row_PaymentType = (TableRow) dialogView.findViewById(R.id.l_paymenttype);
		Row_Bank = (TableRow) dialogView.findViewById(R.id.l_bankname);
		Row_ChequeNumber = (TableRow) dialogView.findViewById(R.id.l_chequenumber);
		Row_ChequeDate = (TableRow) dialogView.findViewById(R.id.l_chequedate);
		Row_Amount = (TableRow) dialogView.findViewById(R.id.l_amount);

		CustomerName = (TextView) dialogView.findViewById(R.id.name);
		CustomerCode = (TextView) dialogView.findViewById(R.id.code);
		DepositAmount = (TextView) dialogView.findViewById(R.id.deposit);
		FilledcansRequested = (TextView) dialogView.findViewById(R.id.cans);


		DepositAmountPaid = (EditText) dialogView.findViewById(R.id.depositpaid);
		ChequeNumber = (EditText) dialogView.findViewById(R.id.chequenumber);
		ChequeDate = (EditText) dialogView.findViewById(R.id.chequedate);
		Filledcans = (EditText) dialogView.findViewById(R.id.filledcans);


		Update = (Button) dialogView.findViewById(R.id.update);
		Cancel = (Button) dialogView.findViewById(R.id.cancel);

		final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
								  int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				//updateLabel();
				String myFormat = "yyyy-MM-dd"; //In which you need put here
				SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
				S_ChequeDate = sdf.format(myCalendar.getTime());
				Log.v("S_ChequeDate",S_ChequeDate);
//yyyy-MM-dd
				String myFormat2 = "dd/MM/yyyy"; //In which you need put here
				SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
				ChequeDate.setText(sdf2.format(myCalendar.getTime()));
				Log.v("ChequeDate",ChequeDate.getText().toString());
			}

		};


		/*private void updateLabel() {
		String myFormat = "MM/dd/yy"; //In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

		edittext.setText(sdf.format(myCalendar.getTime()));
	}
*/
		ChequeDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DatePickerDialog(context, date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});



		Paymentspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
				String payment = item.toString();
				if (payment.equals("Yes")) {
					Row_PaymentType.setVisibility(View.VISIBLE);
					Row_Amount.setVisibility(View.GONE);
					Row_Bank.setVisibility(View.GONE);
					Row_ChequeNumber.setVisibility(View.GONE);
					Row_ChequeDate.setVisibility(View.GONE);
					Row_Amount.setVisibility(View.GONE);
				} else {
					Row_PaymentType.setVisibility(View.GONE);
					Row_Amount.setVisibility(View.GONE);
					Row_Bank.setVisibility(View.GONE);
					Row_ChequeNumber.setVisibility(View.GONE);
					Row_ChequeDate.setVisibility(View.GONE);
					Row_Amount.setVisibility(View.GONE);
				}
			}
		});

		PaymentTypespinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
				String payment = item.toString();
				if(payment.equals("Cheque"))
				{
					//Row_PaymentType.setVisibility(View.VISIBLE);
					Row_Bank.setVisibility(View.VISIBLE);
					Row_ChequeNumber.setVisibility(View.VISIBLE);
					Row_ChequeDate.setVisibility(View.VISIBLE);
					Row_Amount.setVisibility(View.VISIBLE);

				}else if(payment.equals("Cash"))
				{
					Row_Amount.setVisibility(View.VISIBLE);
					Row_Bank.setVisibility(View.GONE);
					Row_ChequeNumber.setVisibility(View.GONE);
					Row_ChequeDate.setVisibility(View.GONE);
				}
			}
		});


		CustomerName.setText(InstallationList.get(Position).getCustomerName());
		CustomerCode.setText(InstallationList.get(Position).getCustomerCode());
		FilledcansRequested.setText(String.format("%s",InstallationList.get(Position).getCansLimit()));
		DepositAmount.setText(String.format("%s", context.getString(R.string.RS)+" "+InstallationList.get(Position).getDepositAmount()+".00"));
		Id = InstallationList.get(Position).getId();

		Cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				alertDialog.dismiss();

			}
		});

		Update.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {



				S_QRCode = spinner.getText().toString();
				S_Payment = Paymentspinner.getText().toString();
				S_Paymenttype = PaymentTypespinner.getText().toString();
				S_Bank = Bankspinner.getText().toString();
				//S_ChequeDate = ChequeDate.getText().toString();
				S_ChequeNumber = ChequeNumber.getText().toString();
				S_Amount = DepositAmountPaid.getText().toString();


			/*	if (DepositAmountPaid.getText().toString().equals("")) {
					toaster.ShowErrorToast("Kindly enter deposit amount !");
				} else*/
				if (S_Payment.equals("Select")) {
					toaster.ShowErrorToast("Please Select Payment !");
				} else if (S_Payment.equals("No")) {

					S_Paymenttype = "";
					S_Bank = "";
					S_ChequeDate = "";
					S_ChequeNumber = "";
					S_Amount = "0";

					if (Filledcans.getText().toString().equals("")) {
						toaster.ShowErrorToast("Kindly enter Filled Cans !");
					} else if (S_QRCode.equals("Select")) {
						toaster.ShowErrorToast("Kindly select QR code option !");
					} else if (network.CheckInternet()) {
						//UpdateInstallation();
					} else {
						alert.showAlertbox(context.getString(R.string.no_network));
					}

				} else if (S_Payment.equals("Yes")) {
					if (S_Paymenttype.equals("Select")){
						toaster.ShowErrorToast("Please Select PaymentType !");
					}else if (S_Paymenttype.equals("Cash")) {
						S_Bank = "";
						S_ChequeDate = "";
						S_ChequeNumber = "";
						if (S_Amount.equals("")) {
							toaster.ShowErrorToast("Please Enter Deposit Amount!");
						} else if (Filledcans.getText().toString().equals("")) {
							toaster.ShowErrorToast("Kindly enter Filled Cans !");
						} else if (S_QRCode.equals("Select")) {
							toaster.ShowErrorToast("Kindly select QR code option !");
						} else if (network.CheckInternet()) {
							//UpdateInstallation();
						} else {
							alert.showAlertbox(context.getString(R.string.no_network));
						}

					} else if (S_Paymenttype.equals("Cheque")) {
						if (S_Bank.equals("Select")) {
							toaster.ShowErrorToast("Please Select Bank Name!");
						} else if (S_ChequeNumber.equals(""))
							toaster.ShowErrorToast("Please Enter Cheque Number!");
						else if (S_ChequeNumber.length() < 6) {
							toaster.ShowErrorToast("Enter valid Cheque Number!");
						}else if (ChequeDate.getText().toString().equals("")) {
							toaster.ShowErrorToast("Please Enter Cheque Date!");
						}
						else if (S_Amount.equals("")) {
							toaster.ShowErrorToast("Please Enter Deposit Amount!");
						} else if (Filledcans.getText().toString().equals("")) {
							toaster.ShowErrorToast("Kindly enter Filled Cans !");
						} else if (S_QRCode.equals("Select")) {
							toaster.ShowErrorToast("Kindly select QR code option !");
						} else if (network.CheckInternet()) {
							//UpdateInstallation();
						} else {
							alert.showAlertbox(context.getString(R.string.no_network));
						}

					}
				}
			}
		});

		alertDialog.show();
	}



/*	private void UpdateInstallation() {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Loading...");
		progressDialog.show();
		APIService service = RetroClient.getApiService();

		Call<Installationupdate> call = service.InstallationUpdate(
				(int) Id,
				Filledcans.getText().toString(),
				S_Amount,
				myshare.getString(Shared.K_Code, ""),
				S_Payment,
				S_Bank,
				S_Paymenttype,
				S_ChequeNumber,
				S_ChequeDate,
				S_QRCode);
		call.enqueue(new Callback<Installationupdate>() {
			@Override
			public void onResponse(Call<Installationupdate> call, Response<Installationupdate> response) {
				progressDialog.dismiss();
				switch (response.body().getResult()) {
					case "Success": {

						AlertDialogue alertnew = new AlertDialogue(context);
						alertnew.showAlertbox("Customer Installation updated Successfully");
						alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
								alertDialog.dismiss();
								((Activity) context).recreate();
							}
						});
						//finish();
						break;
					}
					case "Already Updated": {
						alertDialog.dismiss();
						alert.showAlertbox("Installation already updated! Please check");

						((Activity) context).recreate();
						break;
					}
					default: {

						alert.showAlertbox(context.getString(R.string.connection_slow));

						//Horizontalscrollview.setVisibility(View.GONE);
						//Body_layout.setVisibility(View.GONE);

						((Activity) context).recreate();
						break;
					}
				}
			}

			@Override
			public void onFailure(Call<Installationupdate> call, Throwable t) {
				alertDialog.dismiss();
				progressDialog.dismiss();
				alert.showAlertbox(context.getString(R.string.connection_slow));

			}
		});
	}*/











}










