package sharedpreference;

/**
 * Created by Systems02 on 23-Oct-17.
 */

public class Shared {

    public static final String MyPREFERENCES = "Login_Preference" ;
    public static final String K_Register = "Register";
    public static final String K_RegisterName = "RegisterName";
    public static final String K_RegisterEmail = "RegisterEmail";
    public static final String K_RegisterMobile = "RegisterMobile";
    public static final String K_RegisterType = "RegisterType";
    public static final String K_Login = "Login";
    public static final String K_Usertype = "Usertype";
    public static final String K_Name = "Name";
    public static final String K_Code = "Code";
    public static final String K_Id = "Id";
    public static final String K_Username = "Username";
    public static final String K_Password = "Password";
    public static final String K_Address = "Address";
    public static final String K_Canslimit = "canslimit";
    public static final String K_RegisterID = "RegisterID";

    public static final String K_doorno = "doorno";
    public static final String K_streetname = "streetname";
    public static final String K_area = "area";
}
