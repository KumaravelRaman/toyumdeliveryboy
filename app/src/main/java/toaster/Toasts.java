package toaster;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import com.brainmagic.toyumwater.R;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

/**
 * Created by Systems02 on 06-Nov-17.
 */

public class Toasts {
    private Context context;
    StyleableToast st;


    public Toasts(Context context) {
        this.context = context;

    }
    public void ShowErrorToast(String msg) {

        st = new StyleableToast(context, msg, Toast.LENGTH_SHORT);
        st.setBackgroundColor(context.getResources().getColor(R.color.red));
        st.setTextColor(Color.WHITE);
        st.setMaxAlpha();
        st.show();
    }
    public void ShowSuccessToast(String msg) {

        st = new StyleableToast(context, msg, Toast.LENGTH_SHORT);
        st.setBackgroundColor(context.getResources().getColor(R.color.green));
        st.setTextColor(Color.WHITE);
        st.setMaxAlpha();
        st.show();
    }



}
