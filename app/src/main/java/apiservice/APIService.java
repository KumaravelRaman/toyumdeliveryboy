package apiservice;


import model.Bank.BankNames;
import model.ChangePassword.changepassword;
import model.Contact.SendMessage;
import model.Customer.CustomerInfo.Customerinfo;
import model.Customer.CutomerRefillRequest.CustomerRefill;
import model.Customer.EditCustomerInfo.EditCustomer;
import model.Customer.OrderHistory.CustomerOrderHistory;
import model.Customer.Suggeation_Complaint.suggestionorcomplaint;
import model.Dealer.CustomerDepositUpdate.DepositUpdate;
import model.Dealer.CustomerList.CustomerListDelaer;
import model.Dealer.CustomerLocationUpdate.CustomerLocationupdate;
import model.Dealer.DealerDeliveryHistory.Dealerdeliveryhistory;
import model.Dealer.DeliveryUpdate.DeliveryConfimation;
import model.Dealer.InstallationHistory.Installationhistory;
import model.Dealer.InstallationUpdate.Installationupdate;
import model.Dealer.PendingcanList.PendingCansList;
import model.Dealer.demoupdate.DemoInstallUpdate;
import model.ForgetPassword.ForgetPassword;
import model.Login.LoginResult;
import model.Registration.RegisterResult;
import model.Telecaller.FollowCustomerList.FollowList;
import model.Telecaller.GeneralCustomerList.GeneralList;
import model.Telecaller.UpdateTelecaller.UpdateCustomerTelecaller;
import model.createcustomer.CreateCustomer;
import model.Dealer.demopendinginstalls.DemoPendings;
import model.customerlist.CustomerList;
import model.installationlist.InstallationList;
import model.installationupdate.InstallationUpdate;
import model.referral.ReferralResult;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Systems02 on 10-May-17.
 */

public interface APIService {


    //Registeration
    @FormUrlEncoded
    @POST("api/API/Update_Registeration")
    Call<RegisterResult> REGISTER_RESULT_CALL(
            @Field("name") String name,
            @Field("email") String email,
            @Field("mobile") String mobile,
            @Field("usertype") String usertype,
            @Field("appid") String appid
    );


    //the signin call
    @FormUrlEncoded
    @POST("api/API/Login")
    Call<LoginResult> userLogin(
            @Field("UserName") String Username,

            @Field("Password") String Password,
            @Field("regId") int regId
    );

    // Forget Password
    @FormUrlEncoded
    @POST("api/API/ForgotPassword")
    Call<ForgetPassword> Forget_Password(
            @Field("EmailId") String EmailId
    );

    // Change Password
    @FormUrlEncoded
    @POST("api/API/ChangePassword")
    Call<changepassword> Change_Password(
            @Field("Id") Long Id,
            @Field("OldPassword") String OldPassword,
            @Field("NewPassword") String NewPassword
    );


    // Customer Refill
    @FormUrlEncoded
    @POST("api/API/CustomerRefillRequest")
    Call<CustomerRefill> Customer_Refill(
            @Field("CustomerCode") String CustomerCode,
            @Field("RefillCans") String RefillCans,
            @Field("Remarks") String Remarks

    );

    // Suggestion Or Complaint
    @FormUrlEncoded
    @POST("api/API/SuggestionOrComplaint")
    Call<suggestionorcomplaint> Suggeation_or_Complaint(
            @Field("CustomerName") String CustomerName,
            @Field("CustomerCode") String CustomerCode,
            @Field("Complaints") String Complaints
    );


    // Installation List
    @FormUrlEncoded
    @POST("api/API/InsatllationList")
    Call<InstallationList> Installationlist(
            @Field("Code") String Code,
            @Field("Fromdate") String Fromdate,
            @Field("Todate") String Todate
    );

    // Installation Update
    @FormUrlEncoded
    @POST("api/API/Insatllation_Update")
    Call<Installationupdate> InstallationUpdate(
            @Field("Id") Integer Id,
            @Field("Cans") String Cans,
            @Field("Amount") String Amount,
            @Field("DealerCode") String DealerCode,
            @Field("DepositPaid") String DepositPaid,
            @Field("BankName") String BankName,
            @Field("DepositPayType") String DepositPayType,
            @Field("ChequeNumber") String ChequeNumber,
            @Field("ChequeDate") String ChequeDate,
            @Field("QRcode") String QRcode,
            @Field("Latitude") double Latitude,
            @Field("Longitude") double Longitude
    );

    @FormUrlEncoded
    @POST("api/API/Insatllation_Update")
    Call<InstallationUpdate> NEWInstallationUpdate(
            @Field("Id") Integer Id,
            @Field("Cans") String Cans,
            @Field("CanAmount") double Amount,
            @Field("TotalAmount") double totalAmount,
            @Field("DepositAmount") double depositAmount,
            @Field("DealerCode") String dealerCode,
            @Field("Payment") String payment,
            @Field("PaymentType") String paymentType,
            @Field("Amount") Double amount,
            @Field("BankName") String bankName,
            @Field("ChequeNumber") String chequeNumber,
            @Field("ChequeDate") String chequeDate,
            @Field("CanCover") boolean canCover,
            @Field("Pump") boolean pump,
            @Field("CusCode") String customerCode,
            @Field("CanHandle") boolean canhandle,
            @Field("CapOpener") boolean CanOpener,
            @Field("Swiping") String swiping
//            @Field("BankName") String bankname


    );



    // PendingCansList
    @FormUrlEncoded
    @POST("api/API/PendingCansList")
    Call<PendingCansList> PENDING_CANS_LIST_CALL(
            @Field("Code") String Code,
            @Field("Fromdate") String Fromdate,
            @Field("Todate") String Todate

    );

    // Delivery Confirmation
    @FormUrlEncoded
    @POST("api/API/Dealer_Delivery_Update")
    Call<DeliveryConfimation> DELIVERY_CONFIMATION_CALL(
            @Field("Id") Integer Id,
            @Field("Refill") String Refill,
            @Field("Empty") String Empty,
            @Field("QRcode") String QRcode,
            @Field("Deliveredcans") String Deliveredcans

    );


    // Customer Order History
    @FormUrlEncoded
    @POST("api/API/CustomerOrderHistory")
    Call<CustomerOrderHistory> CUSTOMER_ORDER_HISTORY_CALL(
            @Field("Code") String Code,
            @Field("Fromdate") String Fromdate,
            @Field("Todate") String Todate

    );

    // Dealer delivery History
    @FormUrlEncoded
    @POST("api/API/DealerDeliveryHistory")
    Call<Dealerdeliveryhistory> DEALERDELIVERYHISTORY_CALL(
            @Field("Code") String Code,
            @Field("Fromdate") String Fromdate,
            @Field("Todate") String Todate

    );


    // Customer List for Dealer
    @FormUrlEncoded
    @POST("api/API/CustomerListDealer")
    Call<CustomerListDelaer> CUSTOMER_LIST_DELAER_CALL(
            @Field("DealerCode") String DealerCode

    );


    @FormUrlEncoded
    @POST("api/API/CustomerMailId")
    Call<CustomerList> NEW_CUSTOMER_LIST_DELAER_CALL(
            @Field("DealerCode") String DealerCode

    );

    // Customer Information
    @FormUrlEncoded
    @POST("api/API/CustomerInformation")
    Call<Customerinfo> CUSTOMERINFO_CALL(
            @Field("CustomerCode") String CustomerCode

    );


    // Edit Customer Information
    @FormUrlEncoded
    @POST("api/API/Edit_Customer")
    Call<EditCustomer> EDIT_CUSTOMER_CALL(
            @Field("CustomerCode") String CustomerCode,
            @Field("Mobile") String Mobile,
            @Field("Mobile2") String Mobile2,
            @Field("Email") String Email,
            @Field("Email2") String Email2

    );

    @GET("api/API/BankNameList")
    public Call<BankNames> banknames();

    //InstallationHistory
    @FormUrlEncoded
    @POST("api/API/InstallationHistory")
    Call<Installationhistory> INSTALLATIONHISTORY_CALL(
            @Field("Code") String Code,
            @Field("Fromdate") String Fromdate,
            @Field("Todate") String Todate
    );


    //Update_Customer_Location
    @FormUrlEncoded
    @POST("api/API/Update_Customer_Location")
    Call<CustomerLocationupdate> CUSTOMER_LOCATIONUPDATE_CALL(
            @Field("CustomerCode") String InstalledBy,
            @Field("Latitude") String Latitude,
            @Field("Langitude") String Langitude
    );


    //send email from contact us
    @FormUrlEncoded
    @POST("api/API/Send_Email")
    Call<SendMessage> SEND_MESSAGE_CALL(
            @Field("Name") String Name,
            @Field("Email") String Email,
            @Field("Subject") String Subject,
            @Field("Ordertype") String Ordertype,
            @Field("Message") String Message
    );

    //Customer Deposit Update by Dealer
    @FormUrlEncoded
    @POST("api/API/Update_Customer_Deposit")
    Call<DepositUpdate> DEPOSIT_UPDATE_CALL(
            @Field("CustomerName") String CustomerName,
            @Field("CustomerCode") String CustomerCode,
            @Field("PaymentFor") String PaymentFor,
            @Field("PaymentType") String PaymentType,
            @Field("Amount") String Amount,
            @Field("BankName") String BankName,
            @Field("ChequeNumber") String ChequeNumber,
            @Field("ChequeDate") String ChequeDate,
            @Field("CollectedBy") String CollectedBy

    );


    //ImportedCustomersGeneral
    @FormUrlEncoded
    @POST("api/API/ImportedCustomersGeneral")
    Call<GeneralList> GENERAL_LIST_CALL(
            @Field("TelecallerId") String TelecallerId/*,
                @Field("Alphabet") String Alphabet,
                @Field("From") Integer From,
                @Field("To") Integer To
*/
    );

    @FormUrlEncoded
    @POST("api/API/UpdateCustomerTelecaller")
    Call<UpdateCustomerTelecaller> UPDATE_CUSTOMER_TELECALLER_CALL(
            @Field("id") int id,
            @Field("Name") String Name,
            @Field("Email") String Email,
            @Field("CallStatus") String CallStatus,
            @Field("Appointment") String Appointment,
            @Field("Reason") String Reason,
            @Field("Remarks") String Remarks

    );


    @FormUrlEncoded
    @POST("api/API/ImportedCustomersFollow")
        //ImportedCustomersGeneral
    Call<FollowList> FOLLOW_LIST_CALL(
            @Field("TelecallerId") String TelecallerId
    );


    @FormUrlEncoded
    @POST("api/API/CreateCustomer")
    Call<CreateCustomer> CREATE_CUSTOMER_CALL(

            @Field("CustomerName") String CustomerName,
            @Field("Mobile") String Mobile,
            @Field("Email") String Email,
            @Field("Address") String Address,
            @Field("Pincode") String Pincode,
            @Field("flag") String flag,
            @Field("ReferName") String ReferName,
            @Field("ReferMobile") String ReferMobile,
            @Field("ReferEmail") String ReferEmail,
            @Field("ReferUsertype") String ReferUsertype

    );

    @FormUrlEncoded
    @POST("api/API/AddReferrals")
    Call<ReferralResult> REFERRAL_RESULT_CALL(

            @Field("Name") String Name,
            @Field("Mobile") String Mobile,
            @Field("ReferName") String ReferName,
            @Field("ReferMobile") String ReferMobile
    );



    // DemoPending List
    @FormUrlEncoded
    @POST("api/API/DemoPendingList")
    Call<DemoPendings> DEMO_PENDINGS_CALL(
            @Field("Code") String Code,
            @Field("Fromdate") String Fromdate,
            @Field("Todate") String Todate

    );

    // Demo Intall Update
    @FormUrlEncoded
    @POST("api/API/Demo_Delivery_Update")
    Call<DemoInstallUpdate> DEMO_INSTALL_UPDATE_CALL(
            @Field("Id") Integer Id,
            @Field("CansLimit") Integer CansLimit

    );


    // DemoPending List
    @FormUrlEncoded
    @POST("api/API/DemoHistory")
    Call<DemoPendings> DEMO_HISTORY_CALL(
            @Field("Code") String Code,
            @Field("Fromdate") String Fromdate,
            @Field("Todate") String Todate

    );



      /*  //Change password
        @FormUrlEncoded
        @POST("api/scan/ChangePassword")
        Call<ChangePassword> changepassword(
                @Field("id") int id,
                @Field("Password") String Password,
                @Field("NewPassword") String NewPassword
        );


        //GetReedmedHistory
        @FormUrlEncoded
        @POST("api/scan/GetReedmedHistory")
        Call<RedemptionHistory> viewredemption(
                @Field("id") int id,
                @Field("FromDate") String FromDate,
                @Field("ToDate") String ToDate
        );


        @GET("WebService.asmx/getOrders1")
        Call<testresult> testresult();

        //forgetpassword
        @FormUrlEncoded
        @POST("api/scan/getForgotPassword")
        Call<Forgetpassword> forgetpassword(@Field("email") String email);

        //ViewAccount details
        @FormUrlEncoded
        @POST("api/scan/getAccountDetails")
        Call<ViewAccDetails> getaccdetails(@Field("id") int id);

        //ViewProfile
        @FormUrlEncoded
        @POST("api/scan/ViewProfile")
        Call<Viewprofile> viewprofile(@Field("id") int id);

         //Editprofile
         @FormUrlEncoded
         @POST("api/scan/EditProfile")
         Call<EditProfile> editprofile(
                 @Field("id") int id,
                 @Field("ASCName") String ASCName,
                 @Field("Email") String Email,
                 @Field("Mobile") String Mobile,
                 @Field("State") String State,
                 @Field("City") String City,
                 @Field("Country") String Country
         );


        //view ASC alotted for FSR
        @FormUrlEncoded
        @POST("api/scan/FSRView")
        Call<FSRViewASC> viewascforfsr(
                @Field("FSRid") int FSRid
        );


        //view ASC scan History
        @FormUrlEncoded
        @POST("api/scan/ASCScanHistory")
        Call<ASCScanHistory> ASCScanHistory(
                @Field("ScannedBy") int ScannedBy,
                @Field("FromDate") String FromDate,
                @Field("ToDate") String ToDate
        );
  */
}
