package corporate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

public class Corporate_DC_Activity extends AppCompatActivity {
    Button Done,Scan;
    EditText Cans;
    TableLayout Content_layout;
    ImageView Back,Home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corporate_dc);
        Done = (Button)findViewById(R.id.done);
        Scan = (Button)findViewById(R.id.scan);
        Cans = (EditText) findViewById(R.id.numder_cans);
        Content_layout = (TableLayout) findViewById(R.id.content_layout);
        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        Scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Content_layout.setVisibility(View.VISIBLE);
            }
        });

        Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cans.setText("");
                Toast.makeText(Corporate_DC_Activity.this, "Order Processed Successfully", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
